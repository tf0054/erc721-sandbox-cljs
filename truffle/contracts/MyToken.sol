pragma solidity ^0.4.23;

import "zeppelin-solidity/contracts/token/ERC721/ERC721Token.sol";
import "zeppelin-solidity/contracts/ownership/Ownable.sol";

contract MyToken is ERC721Token("TokhimoToken", "TKMO"), Ownable {
  uint256 internal nextTokenId = 0;
  address public serverAddr = 0xDE560C41FC36AA02d0Cb8161F15747701A30015d;

  mapping(uint256 => bool) internal bought;
  // Optional mapping for token URIs
  mapping(uint256 => address) internal tokenHost;

  // Optional mapping for token URIs
  mapping(uint256 => uint256) internal tokenPrice;

  mapping(uint256 => bool) internal tokenBurned;

  mapping(uint256 => bool) internal paid;
  mapping(uint256 => bool) internal ignoreDue;
  mapping(uint256 => bool) internal sellerReviewed;
  mapping(uint256 => bool) internal buyerReviewed;

  mapping(uint256 => uint256) internal tokenStart;

  event payTransfer(
    uint256 t,
    address s,
    bool b,
    bool p);

  //  function mint() external {
  //    uint256 tokenId = nextTokenId;
  //    nextTokenId = nextTokenId.add(1);
  //    super._mint(msg.sender, tokenId);
  //  }

  function getHost(uint256 _tokenId) public view returns (address){
    //require(exists(_tokenId));
    return tokenHost[_tokenId];
  }

  function _setHost(uint256 _tokenId, address _host) internal {
    //require(exists(_tokenId));
    tokenHost[_tokenId] = _host;
  }

  function getPrice(uint256 _tokenId) public view returns (uint256){
    //require(exists(_tokenId));
    return tokenPrice[_tokenId];
  }

  function _setPrice(uint256 _tokenId, uint256 _price) internal {
    tokenPrice[_tokenId] = _price;
  }

  function setPrice(uint256 _tokenId, uint256 _price) public {
    //require(exists(_tokenId));
    address orgOwner = ownerOf(_tokenId);
    if ((msg.sender == orgOwner) && ! bought[_tokenId])
      _setPrice(_tokenId, _price);
  }

  function _payTransfer(uint256 _tokenId, uint hostP, uint tokhP) internal {
        paid[_tokenId] = true;
        if (!tokenHost[_tokenId].send(tokenPrice[_tokenId] * hostP / 100)) require(false);
        if (!serverAddr.send(tokenPrice[_tokenId] * tokhP / 100)) require(false);
        delete bought[_tokenId];
        delete tokenStart[_tokenId];
        delete sellerReviewed[_tokenId];
        delete buyerReviewed[_tokenId];
  }

  function setIgnoreDue(uint256 _tokenId, bool f) public onlyOwner {
    ignoreDue[_tokenId] = f;
  }

  function payByOverdue(uint256 _tokenId, uint hostP, uint tokhP) public onlyOwner {
      if (!ignoreDue[_tokenId]) {
          emit payTransfer(_tokenId, msg.sender, sellerReviewed[_tokenId], buyerReviewed[_tokenId]);
          _payTransfer(_tokenId, hostP, tokhP);
      }
  }

  function setReviewed(uint256 _tokenId) public {
    //require(exists(_tokenId));
    if (!paid[_tokenId]) {
      if (msg.sender == tokenHost[_tokenId]) {
        sellerReviewed[_tokenId] = true;
      } else if (msg.sender == ownerOf(_tokenId)) {
        buyerReviewed[_tokenId] = true;
      } else {
        require(false);
      }

      if (sellerReviewed[_tokenId] && buyerReviewed[_tokenId] && !paid[_tokenId]) {
        emit payTransfer(_tokenId, msg.sender, sellerReviewed[_tokenId], buyerReviewed[_tokenId]);
        _payTransfer(_tokenId, 97, 3);
      }
    }
  }

  function checkPaid(uint256 _tokenId) public view returns (bool) {
    return (paid[_tokenId]);
  }

  function getStartEpocSec(uint256 _tokenId) public view returns (uint256) {
    return (tokenStart[_tokenId]);
  }

  function mintWithMsg(string _message, uint256 _price, address r) public onlyOwner {
    uint256 tokenId = nextTokenId;
    nextTokenId = nextTokenId.add(1);
    super._mint(r, tokenId);
    super._setTokenURI(tokenId, _message);
    _setPrice(tokenId, _price);
    _setHost(tokenId, r);
    // --
    sellerReviewed[tokenId] = true;
  }

  function buyToken(uint256 _tokenId, uint _startESec) payable external {
    if (msg.value < tokenPrice[_tokenId]) require(false);

    address orgOwner = ownerOf(_tokenId);
    if (serverAddr != orgOwner) {
      bought[_tokenId] = true;
      tokenStart[_tokenId] = _startESec;
    }
  }

  function moveToken(uint256 _tokenId, address r) public onlyOwner {
    address orgOwner = ownerOf(_tokenId);
    //if ((serverAddr != orgOwner) && bought[_tokenId])
    if (serverAddr != orgOwner) {
      transferFrom(orgOwner, r, _tokenId);
    }
  }

  function setTokenURI(uint256 _tokenId, string _message) external onlyOwnerOf(_tokenId) {
    super._setTokenURI(_tokenId, _message);
  }

  function officialBurn(uint256 _tokenId) external {
    address orgOwner = ownerOf(_tokenId);
    delete tokenHost[_tokenId];
    delete tokenPrice[_tokenId];
    super._burn(orgOwner, _tokenId);
  }

  function burn(uint256 _tokenId) external {
    delete tokenHost[_tokenId];
    delete tokenPrice[_tokenId];
    tokenBurned[_tokenId] = true;
    moveToken(_tokenId, serverAddr);
  }
}
