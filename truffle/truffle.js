var WalletProvider = require("truffle-wallet-provider");

var keystore = require('fs').readFileSync('../wallet/UTC--2018-06-28T10-45-28.497000000Z--de560c41fc36aa02d0cb8161f15747701a30015d.json').toString();
var pass = "lala0054";
var wallet = require('ethereumjs-wallet').fromV3(keystore, pass);

console.log("Creator addr: "+wallet.getAddressString());

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*" // Match any network id
    },
    rinkeby: {
      provider: function() {
        return new WalletProvider(wallet, "https://rinkeby.infura.io/7QYvMaW8qdIAPws0c8E7")
      },
      network_id: 4
    }
  }

};
