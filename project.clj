(defproject hello-webpack "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.339" :exclusions [commons-codec]]
                 [reagent "0.8.1"]
                 [re-frame "0.10.5"]
                 [day8.re-frame/http-fx "0.1.6"]
                 [day8.re-frame/async-flow-fx "0.0.11"]
                 [re-frame-datatable "0.6.0"]
                 [com.andrewmcveigh/cljs-time "0.5.2"]
                 ;;
                 [cljsjs/material-ui-icons "1.1.0-1"]
                 [cljsjs/material-ui "1.4.0-0"]
                 [cljs-react-material-ui "0.2.55"]
                 ;;
                 [cljsjs/quill "1.1.0-3"]
                 [macchiato/hiccups "0.4.1"]
                 ;;
                 [compojure "1.6.1"]
                 [ring "1.6.3"]
                 [ring/ring-defaults "0.3.2"]
                 [ring/ring-json "0.4.0" :exclusions [cheshire]]
                 [ring-logger "1.0.1" :exclusions [log4j]]
                 [resilva87/ring-gzip-middleware "0.2.0"]
                 [cheshire "5.8.0" :exclusions [com.fasterxml.jackson.core/jackson-core]]
                 [codax "1.3.0"]
                 #_[yogthos/config "0.8"]
                 [clj-http "3.9.1"]
                 [jarohen/chime "0.2.2" :exclusions [org.clojure/core.async]]
                 [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                                    javax.jms/jms
                                                    com.sun.jdmk/jmxtools
                                                    com.sun.jmx/jmxri]]
                 [org.slf4j/slf4j-simple "1.7.12" :exclusions [org.slf4j/slf4j-api]]
                 [org.web3j/core "3.5.0" :exclusions [com.fasterxml.jackson.core/jackson-annotations
                                                      com.fasterxml.jackson.core/jackson-databind]]
                 ;;
                 [com.algolia/algoliasearch "2.17.3"]]

  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-ring "0.12.4"]]

  :min-lein-version "2.5.3"

  :ring {:handler httpd.server-handler/dev-app
         ;; the :init config key is very helpful especially in the context
         ;; of production
         ;; :init example.server/init
         }

  :source-paths ["src/clj"]
  :java-source-paths ["src/java"]
  :resource-paths ["target/classes" "resources"]
  ;; the below wasnt called when youd use "lein javac"
  :prep-tasks ["javac"]

  :figwheel {:init     user/start-server
             :destroy  user/stop-server
             ;;
             :css-dirs ["resources/public/css"]}

   :aliases {"package" ["do" "clean"
                       ["cljsbuild" "once" "min"]
                       ["ring" "uberjar"]]}

  :profiles
  {:dev
   {:dependencies [[binaryage/devtools "0.9.10"]
                   [day8.re-frame/re-frame-10x "0.3.3"]
                   [figwheel-sidecar "0.5.16"]
                   [ring-server "0.5.0"]]
     
    :source-paths ["dev"]
    :clean-targets ^{:protect false} ["resources/public/js/compiled"]

    :plugins      [[lein-figwheel "0.5.16"]
                   [lein-ancient "0.6.15"]]}}

  :cljsbuild
  {:builds
   [{:id           "dev"
     :source-paths ["src/cljs"]
     :figwheel     {:on-jsload " hello-webpack.core/mount-root "}
     :compiler     {:main                 hello-webpack.core
                    :output-to            "resources/public/js/compiled/app.js"
                    :output-dir           "resources/public/js/compiled/out"
                    :asset-path           "js/compiled/out"
                    :language-in          :ecmascript5
                    :install-deps         true
                    :source-map-timestamp true
                    :external-config      {:devtools/config {:features-to-install :all}}
                    :infer-externs        true
                    :npm-deps             false
                    :foreign-libs         [{:file     "webpack/dist/index_bundle.js"
                                            :provides ["shadow-cljs"]
                                            }]
                    :closure-defines      {"re_frame.trace.trace_enabled_QMARK_" true}
                    ;;:preloads             [devtools.preload]
                    :preloads             [day8.re-frame-10x.preload]
                    }
     }

    #_{:id           "min"
     :source-paths ["src/cljs"]
     :compiler     {:main            test-npm-deps.core
                    :output-to       "resources/public/js/compiled/app.js"
                    :optimizations   :advanced
                    :language-in     :ecmascript5           ; react 16 need es5 or above
                    :externs         ["resources/react.ext.js"] ; a externs from react project
                    :install-deps    true                   ; mandatory
                    :npm-deps        {:react              "16.2.0" ; mandatory
                                      :react-dom          "16.2.0"
                                      :create-react-class "15.6.2"}
                    :closure-defines {goog.DEBUG false}
                    :pretty-print    false}}

    ]}

  )
