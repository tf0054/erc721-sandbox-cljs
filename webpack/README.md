# npm-module-example

CLJS for JS devs

### Instructions

```
./node_modules/.bin/shadow-cljs compile npm
```

You need either `java` installed or `yarn global add node-jre`.

After that has finished compiling you may either run
```
npm run webpack
./node_modules/.bin/webpack
```

# Production mode

```
vi src/module/index.js
(1) shadhow.cljs compile npm
vi src/js/index.js
(2) npm run webpack
vi src/cljs/hello_webpack/core.cljs
(3) clj -m cljs.main -co build.edn -v -c  ::from https://clojurescript.org/guides/webpack
```
