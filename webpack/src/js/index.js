//import ContractJSON from '../../../truffle/build/contracts/MyToken.json';
//window.ContractJSON = ContractJSON; 
window.Drizzle = require("shadow-cljs/demo.foo");

window.getContractJSON = function (url) {
  var request = require('xhr-request')
 
  request(url,
    { json: true },
    function (err, data) {
      if (err) throw err
	  
      console.log("Imported " + url + " as window.ContractJSON");
      window.ContractJSON = data;
    });
}

console.log("Imported npm modules ns to cljs including Drizzle");
//console.log(window.Drizzle);
//console.log(window.ContractJSON);
