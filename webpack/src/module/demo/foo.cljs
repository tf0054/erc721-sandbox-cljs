(ns demo.foo
  (:require #_[shadow.dom :as dom]
            #_[shadow.markup.css :as css :refer (defstyled)]
            ["drizzle" :as drizzle :refer [Drizzle generateStore]]
            #_["drizzle-react" :as drizzleReact :refer (DrizzleProvider)]
            ))

(defn ^:export getDrizzle []
      drizzle
      )

#_(defn ^:export getDrizzleReact []
      drizzleReact
      )
