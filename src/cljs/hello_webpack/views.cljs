(ns hello-webpack.views
  (:require
    [reagent.core :as r]
    [re-frame.core :as re-frame]
    [cljsjs.material-ui]
    [cljs-react-material-ui.core :refer [get-mui-theme color]]
    [cljs-react-material-ui.reagent :as ui]
    [cljs-react-material-ui.icons :as ic]
    [hello-webpack.subs :as subs]
    [hello-webpack.events :as e]
    [hello-webpack.ethereum.events :as ee]
    [hello-webpack.search.events :as se]
    [hello-webpack.ajax :as ajax]
    [hello-webpack.utils :as u]
    [hello-webpack.drizzle :as drizzle]
    [hello-webpack.reactable :as rtable]
    [hello-webpack.components.firstPage :as firstP]
    [hello-webpack.components.secondPage :as secondP]
    [hello-webpack.components.approval :as approvP]
    [hello-webpack.components.googleLogin :as googleP]
    [hello-webpack.components.metamaskLogin :as metamaskP]
    [hello-webpack.components.search :as searchP]
    [hello-webpack.components.editor :as editorP]
    [hello-webpack.components.header :as header]
    [hello-webpack.components.footer :as footer]
    [hello-webpack.components.detail :as detailP]
    )
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(defn main-panel []
  (let [total (re-frame/subscribe [::subs/total])
        page (re-frame/subscribe [::subs/page])
        drawer-open (re-frame/subscribe [::subs/drawerOpen])
        account (re-frame/subscribe [::subs/account])
        email (re-frame/subscribe [::subs/profile :email])]
    (fn []
      (re-frame/dispatch [::ee/checkMetamask])
      [ui/mui-theme-provider
       {:theme (get-mui-theme {:palette {;:type "dark"
                                         }})}
       [:div #_{:on-click #(if (not @drawer-open)
                             (re-frame/dispatch [::e/drawerToggle]))}
        #_(u/header account email)
        [header/view]
        ;;
        [:div
         (case @page
           "googleLogin" [googleP/view]
           ;; "metamaskLogin" [metamaskP/view]
           "approval" [approvP/view]
           "first" [firstP/view]
           "second" [secondP/view]
           "search" [searchP/view]
           "editor" [editorP/view]
           "detail" [detailP/view]
           "booting" [:div (str "Booting.. please wait")]
           [:div (str "ERROR: " @page)])]
        ;;
        [footer/view]
        ;;
        [:div
         [ui/drawer {:open              @drawer-open
                     :docked            false
                     :on-request-change #(re-frame/dispatch [::e/drawerToggle])
                     }
          ;;
          #_[ui/app-bar {:title                         "menu"
                       :style                         {:text-align "center"}
                       :icon-element-left             (r/as-element [ui/icon-button
                                                                     [ic/navigation-close
                                                                      {:style {:color "white"}}]])
                       :on-left-icon-button-touch-tap #(re-frame/dispatch [::e/drawerToggle])}
           [ui/toolbar
            [ui/typography {:variant "title"
                            :color "inherit"} "Menu"]
            ]
           ;<Toolbar>
           ;<IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
           ;<MenuIcon />
           ;</IconButton>
           ;<Typography variant="title" color="inherit" className={classes.flex}>
           ;News
           ;</Typography>
           ;<Button color="inherit">Login</Button>
           ;</Toolbar>
           ]
          [ui/menu-item {:on-click #(do
                                      (re-frame/dispatch [::e/drawerToggle])
                                      (re-frame/dispatch [::e/updateProfiles])
                                      (re-frame/dispatch [::se/setSearchWord ""])
                                      (re-frame/dispatch [::u/pageChange "search"])
                                      )}
           [ic/add-box]
           [ui/list-item-text {:primary "SEARCH"}]]
          [ui/menu-item {:on-click #(do
                                      (re-frame/dispatch [::e/drawerToggle])
                                      (re-frame/dispatch [::e/updateProfiles])
                                      (re-frame/dispatch [::u/pageChange "second"])
                                      )}
           #_(log (clojure.string/join "\n" (js-keys js/MaterialUI)))
           [ic/contact-mail]
           [ui/list-item-text {:primary "TOKEN LIST"}]]
          [ui/menu-item {:on-click #(do
                                      (re-frame/dispatch [::e/drawerToggle])
                                      (re-frame/dispatch [::u/pageChange "editor"])
                                      (re-frame/dispatch [::ajax/getContentsInfo])
                                      )}
           [ic/edit]
           [ui/list-item-text {:primary "EDITOR"}]]
          [ui/menu-item {:on-click #(do
                                      (re-frame/dispatch [::e/drawerToggle])
                                      (re-frame/dispatch [::rtable/mkDebugTableData])
                                      (re-frame/dispatch [::u/pageChange "first"])
                                      )}
           [ic/developer-board]
           [ui/list-item-text {:primary "(DEBUG)"}]]
          ]]
        ]]
))
)
