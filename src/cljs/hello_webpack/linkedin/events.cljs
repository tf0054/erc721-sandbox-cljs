(ns hello-webpack.linkedin.events
  (:require [re-frame.core :as re-frame]
            [hello-webpack.ajax :as a]
            [hello-webpack.utils :as u]
            [ajax.core :as ajax])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(def bchannel (aset (js/BroadcastChannel. "Tokhimo0")
                    "onmessage"
                    (fn [x]
                      (log "Broadcased: " x)
                      (re-frame/dispatch [::disableLinkedIn])
                      (re-frame/dispatch [::a/loadProfile])
                      )))

(re-frame/reg-event-db
  ::disableLinkedIn
  (fn [db [_ content]]
    (assoc-in db [:linkedIn :button] false)
    ))

(re-frame/reg-event-db
  ::setLinkedInUrl
  (fn [db [_ content]]
    (assoc-in db [:linkedIn :url] (:result content))
    ))

(re-frame/reg-event-fx
  ::getLinkedInUrl
  (fn [{:keys [db]} [_ _]]
    (log (str ::getLinkedInUrl " http "))
    {:db         db
     :http-xhrio {:method          :get
                  :uri             (str "/linkedin")
                  :params          {:address (get-in db [:profile :email])}
                  :timeout         3000
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true}) ;; IMPORTANT!: You must provide this.
                  :on-success      [::setLinkedInUrl]
                  :on-failure      [::u/apiAccessFailed]}})
  )