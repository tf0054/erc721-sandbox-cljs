(ns hello-webpack.components.googleLogin
  (:require [re-frame.core :as re-frame]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.subs :as subs]
            [hello-webpack.events :as e]
            [hello-webpack.ethereum.events :as ee]
            [hello-webpack.utils :as u]))

(defn view []
  (let []
    [:div
     [:div {:class "note"}
      "Firstly please login Google using the button below."]
     ]
     )
    )
