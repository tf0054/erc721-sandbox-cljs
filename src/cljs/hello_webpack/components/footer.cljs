(ns hello-webpack.components.footer
  (:require [re-frame.core :as re-frame]
            [cljs-react-material-ui.reagent :as ui]
            [hello-webpack.subs :as subs]
            [hello-webpack.events :as e]))

(defn view []
  (let [page (re-frame/subscribe [::subs/page])]
    [:div
     [:hr]
     (if (not (or (= @page "googleLogin")
                  (= @page "booting")) )
       [ui/button {:children "menu"
                   :color "primary"
                   :variant  "contained"
                   :style    {:text-weight "bold" :margin-left "10px"}
                   :on-click #(re-frame/dispatch [::e/drawerToggle])}] )
     ]
  ))