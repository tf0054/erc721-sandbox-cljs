(ns hello-webpack.components.metamaskLogin
  (:require [re-frame.core :as re-frame]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.subs :as subs]
            [hello-webpack.events :as e]
            [hello-webpack.ethereum.events :as ee]
            [hello-webpack.utils :as u]))

;; Currently not used (metamask errors are notified via dialog

(defn view []
  (let []
    [:div
     [:div {:class "note"}
     "Firstly please login Metamask."]
     [:br]
     [ui/button {:style    {:color "white" :text-weight "bold" :margin-bottom "5px"}
                        :on-click #(set! (.-href window.location) "/")} "Check"]
     ] )
    )
