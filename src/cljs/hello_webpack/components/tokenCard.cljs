(ns hello-webpack.components.tokenCard
  (:require [reagent.core :as r]
            [re-frame.core :as re-frame]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.events :as e]
            [hello-webpack.utils :as u]

            )
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(defn view []
  (let []
    (fn [x times]
      [ui/card {:key (str (:descriptionId x) "_card")
                :style {:margin "40px 20px 20px 20px"}}
       [ui/card-header {:key (str (:descriptionId x) "_header")
                        :title (:title x)
                        :subheader "subheader is here"}]
       [:img {:key (str (:descriptionId x) "_img")
              :src (:imageUrl x)}]
       [ui/card-content {:key (str (:descriptionId x) "_body")}
        [ui/typography (str x)] ]
       (for [y times]
         [ui/card-content {:key (str (:descriptionId x) "_" (:objectID y) "_txt")}
          [ui/typography {:key (str (:descriptionId x) "_" (:objectID y))}
           (str y)]
          [:button {:key      (str (:descriptionId x) "_" (:objectID y) "_buy")
                    :id       (str "tokhimo-buy-" (:objectID y))
                    :on-click #(do (re-frame/dispatch [::e/setSelectedCourse x times])
                                   (re-frame/dispatch [::u/pageChange "detail"]))}
           (str "detail (" (:objectID y) ")")]
          ])
       ]
      )
  ))