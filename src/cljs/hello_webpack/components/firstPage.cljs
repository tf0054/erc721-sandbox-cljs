(ns hello-webpack.components.firstPage
  (:require [re-frame.core :as re-frame]
            [re-frame-datatable.core :as dt]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.subs :as subs]
            [hello-webpack.events :as e]
            [hello-webpack.ethereum.events :as ee]
            [hello-webpack.ethereum.utils :as eu]))

(defn gmail-like-pagination [db-id data-sub]
  (let [pagination-state (re-frame/subscribe [::re-frame-datatable.core/pagination-state db-id data-sub])]
    (fn []
      (let [{:keys [::re-frame-datatable.core/cur-page ::re-frame-datatable.core/pages]} @pagination-state
            total-pages (count pages)
            next-enabled? (< cur-page (dec total-pages))
            prev-enabled? (pos? cur-page)]

        [:div
         [:div {:style {:display      "inline-block"
                        :margin-right ".5em"}}
          [:strong
           (str (inc (first (get pages cur-page))) "-" (inc (second (get pages cur-page))))
           ]
          [:span " of "]
          [:strong (inc (second (last pages)))]]

         [:div.ui.pagination.mini.menu
          [:a.item
           {:on-click #(when prev-enabled?
                         (re-frame/dispatch [::re-frame-datatable.core/select-prev-page db-id @pagination-state]))
            :class    (when-not prev-enabled? "disabled")}
           [:i.left.chevron.icon]]

          [:a.item
           {:on-click #(when next-enabled?
                         (re-frame/dispatch [::re-frame-datatable.core/select-next-page db-id @pagination-state]))
            :class    (when-not next-enabled? "disabled")}
           [:i.right.chevron.icon]]]]))))

(defn view []
  (let [address (re-frame/subscribe [::subs/account])
        balance (re-frame/subscribe [::subs/balance])
        network (re-frame/subscribe [::subs/profile :network])
        email (re-frame/subscribe [::subs/profile :email])
        image (re-frame/subscribe [::subs/profile :imageUrl])
        gname (re-frame/subscribe [::subs/profile :givenName])
        fname (re-frame/subscribe [::subs/profile :familyName])
        serverAddr (re-frame/subscribe [::subs/serverProfile :address])
        serverBalance (re-frame/subscribe [::subs/serverProfile :balance])
        serverNetwork (re-frame/subscribe [::subs/serverProfile :network])
        total (re-frame/subscribe [::subs/total])
        tokenAddr (re-frame/subscribe [::subs/tokenProfile :address])
        tokenBalance (re-frame/subscribe [::subs/tokenProfile :balance])
        ;;
        tokenId (re-frame/subscribe [::subs/tokenId])
        tokenMsg (re-frame/subscribe [::subs/tokenMsg])
        ;tokenDebugTableData (re-frame/subscribe [::subs/tokenDebugTableData])
        allMsgs (re-frame/subscribe [::subs/allMsgs])
        ]

    [:div
     [:h1 "DEBUG DASHBOARD"]
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [:div "Detected server"]
      [:div
       [:div "Address: " [:a {:href (str "https://rinkeby.etherscan.io/address/" @serverAddr)
                              :target "_blank"} @serverAddr]]
       [:div (str "Balance: " (eu/from-wei @serverBalance) " (" @serverBalance ")")]
       [:div (str "Network: " @serverNetwork " (" (if (= @network @serverNetwork)
                                                   "matched"
                                                   "not matched") ")")]
       ]]
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [:div "Detected Token"]
      [:div
       [:div "Address: " [:a {:href (str "https://rinkeby.etherscan.io/address/" @tokenAddr)
                              :target "_blank"} @tokenAddr]]
       [:div (str "Balance: " (eu/from-wei @tokenBalance) " (" @tokenBalance ")")]
       [:div "Issued Token(s): " [:a {:href (str "https://rinkeby.etherscan.io/token/" @tokenAddr)
                                           :target "_blank"} @total]]
       ]]
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [:div "Detected Metamask"]
      [:div
       [:img {:src @image}]
       [:div (str "Name: " @fname " " @gname)]
       [:div (str "email: " @email)]
       [:hr]
       [:div "Address: " [:a {:href (str "https://rinkeby.etherscan.io/address/" @address)
                                   :target "_blank"} @address]]
       [:div (str "Balance: " (eu/from-wei @balance) " (" @balance ")")]
       #_[:div (str "linkedIn: " @linkedInProfile)]
       ]]

     [dt/datatable
      :datatable-test
      [::subs/tokenDebugTableData]
      (hello-webpack.reactable/defDebugTableData)
      {::dt/pagination    {::dt/enabled? true
                           ::dt/per-page 5}
       ::dt/table-classes ["ui" "table" "celled"]}]
     [gmail-like-pagination :datatable-test [::subs/tokenDebugTableData]]

     #_[ui/raised-button {:style   {:color "white" :text-weight "bold"} :primary true
                        :on-click #(re-frame/dispatch [::e/sync-db 10])} "inc"]

     #_[ui/raised-button {:style   {:color "white" :text-weight "bold"}
                        :on-click #(re-frame/dispatch [::ee/mytokenSetApprovalForAll])} "Approve"]
     ; window.open("https://www.w3schools.com", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
     [:br]
     (let [tId 0]
       [:button {;; :key      (str (:descriptionId x) "_" (:objectID y) "_buy")
               ;; :id       (str "tokhimo-buy-" (:objectID y))
               :on-click (fn []
                           (re-frame/dispatch [::ee/mytokenSetReviewed tId]))}
      (str "DEBUG: Finished(" tId ")")]
       [:button {;; :key      (str (:descriptionId x) "_" (:objectID y) "_buy")
                 ;; :id       (str "tokhimo-buy-" (:objectID y))
                 :on-click (fn []
                             (re-frame/dispatch [::ee/mytokenCheckPaid tId]))}
        (str "check (" tId ")")]
       )
     [:button {;; :key      (str (:descriptionId x) "_" (:objectID y) "_buy")
               ;; :id       (str "tokhimo-buy-" (:objectID y))
               :on-click (fn []
                           (re-frame/dispatch [::ee/mytokenGetStartEpocSec 0]))}
      (str "getStartEpocSec (" 0 ")")]
     [:button {;; :key      (str (:descriptionId x) "_" (:objectID y) "_buy")
               ;; :id       (str "tokhimo-buy-" (:objectID y))
               :on-click (fn [] (re-frame/dispatch [::ee/mytokenSetApprovalForAll]))}
      (str "approve")]
     [:button {;; :key      (str (:descriptionId x) "_" (:objectID y) "_buy")
               ;; :id       (str "tokhimo-buy-" (:objectID y))
               :on-click (fn [] (re-frame/dispatch [::ee/mytokenGetPrice 2]))}
      (str "getPrice")]
     #_[ui/text-field {:type                "text"
                     :value               @tokenId
                     :floating-label-text "TokenId for set"
                     :name                "tokenId"
                     ;;:hint-text           "username"
                     :on-change           #(re-frame/dispatch [::e/setTokenId (-> % .-target .-value)])}]
     #_[ui/text-field {:type                "text"
                     :value               @tokenMsg
                     :floating-label-text "Message for set"
                     :name                "tokenMsg"
                     :on-change #(re-frame/dispatch [::e/setTokenMsg (-> % .-target .-value)])}]

     [ui/button {:style   {:color "white" :text-weight "bold" :margin-left "10px"}
                        :on-click #(re-frame/dispatch [::ee/mytokenSetUrlByIndex @tokenId @tokenMsg])} "set"]
     [:br]
     ;; Regarding debug card
     [ui/button {:style   {:color "white" :text-weight "bold"} :primary true
                        :on-click #(re-frame/dispatch [::e/updateAll @total true])} "initdb"]
     [ui/button {:style   {:color "white" :text-weight "bold" :margin-left "10px"} :primary true
                        :on-click #(re-frame/dispatch [::e/updateAll @total false])} "refreshdb"]

     [:br]
     ;; Regarding menu test
     [ui/button {:style   {:color "white" :text-weight "bold"}
                        :on-click #(re-frame/dispatch [::e/drawerToggle])} "menu"]
     #_[ui/raised-button {:style    {:color "white" :text-weight "bold" :margin-left "10px"}
                        :on-click #(re-frame/dispatch [::ee/mytokenIsApprovedForAll @serverAddr])} "getApproved" ]
     #_[ui/raised-button {:style    {:color "white" :text-weight "bold" :margin-left "10px"}
                        :on-click #(re-frame/dispatch [::e/getGoogleProfile])} "googleProfile" ]
     #_[ui/raised-button {:style    {:color "white" :text-weight "bold" :margin-left "10px"}
                        :on-click #(re-frame/dispatch [::e/loadProfile @address])} "LoadProfile" ]
     ;;
     #_[ui/card {:style {:margin "40px 20px 20px 20px"}}
      [ui/card-title "Hosting Timeslot(s)"]
      [ui/card-text
       (if (not (empty? @tokenDebugTableData))
         [rt/table {:style        {:margin-left "10px"}
                    :data         @tokenDebugTableData
                    :itemsPerPage 10}
          ]
         [:span "No timeslot(s) was found (as seller)"])
       ]]

     [ui/card {:style {:margin "40px 20px 60px 20px"}}
      [:div
       [:pre @allMsgs]
       ]]
     ])
    )
