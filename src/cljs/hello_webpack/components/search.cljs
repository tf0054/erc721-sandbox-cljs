(ns hello-webpack.components.search
  (:require [re-frame.core :as re-frame]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.store :as s]
            [hello-webpack.subs :as subs]
            [hello-webpack.utils :as u]
            [hello-webpack.search.events :as se]
            [hello-webpack.components.tokenCard :as card])
  (:require-macros
    [hello-webpack.macros :refer [log]]))

(defn view []
  (let [sword (re-frame/subscribe [::subs/search :word])
        cardsData (re-frame/subscribe [::subs/search :cards])
        profiles (re-frame/subscribe [::subs/profilesAll])
        ]
    #_(log (str "MetamaskCheck: " (u/checkMetamask)))
    [:div
     [:div {:class "note"} "This table is for showing search results."]
     [ui/text-field {:type                "text"
                     :value               @sword
                     :label "Word for searching"
                     :name                "searchWord"
                     ;;:hint-text           "username"
                     :on-change           #(re-frame/dispatch [::se/setSearchWord (u/e-val %)])}]
     ;;
     (doall (for [x @cardsData]
       (do (let [cardInfo (-> (first x)
                              (merge (get @profiles (keyword (:hostAddr (first x)))))
                              (dissoc :startTime :endTime :id :email :network :_highlightResult))
                 times (remove nil? (map (fn [y] {:objectID  (js/parseInt (:objectID y))
                                                  :eventId   (:eventId y)
                                                  :startTime (:startTime y)
                                                  :endTime   (:endTime y)})
                                         x))]
             ^{:key (:descriptionId cardInfo)} [card/view cardInfo times]))
       ))
     ]
    )
  )