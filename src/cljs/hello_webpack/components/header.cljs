(ns hello-webpack.components.header
  (:require [re-frame.core :as re-frame]
            [cljs-react-material-ui.reagent :as ui]
            [hello-webpack.subs :as subs]
            [hello-webpack.events :as e]
            [hello-webpack.ajax :as ajax]
            [hello-webpack.components.dialog :as d]
            [hello-webpack.components.dialogs :as ds]))

(defn view []
  (let [page (re-frame/subscribe [::subs/page])
        account (re-frame/subscribe [::subs/account])
        email (re-frame/subscribe [::subs/profile :email])
        editProfile (re-frame/subscribe [::subs/dialog :editProfile])]
    [:div {:id "header"}
     [:div {:class "bg-help"}
      [:div {:class "inBox"}
       [:h1 {:id "logo"
             :style {:display "inline"}}
        "Teleconference slots seller"]
       (if (not (nil? @email))
         [ui/button {:children "profile"
                     :variant  "contained"
                     :style    {:margin "10px 30px 0 0"
                                :float "right"}
                     :on-click #(re-frame/dispatch [::e/setDialog :editProfile true])}])
       [:div [:span {:id "address"} @account]
        (if (not (nil? @email))
          [:span {:id "email"} (str " (" @email ")")])]
       #_[:hr {:class "hidden"}]
       ]
      ]
     ;;
     [d/common-dialog {:title    "Edit profile"
                       :contents ds/editProfile
                       :style    {:width  "500px"
                                  :height "600px"}
                       :open     @editProfile
                       :ok-label "Save"
                       :ok       #(do
                                    (re-frame/dispatch [::ajax/saveProfile])
                                    (re-frame/dispatch [::e/setDialog :editProfile false]))
                       ; :cancel   #(re-frame/dispatch [::e/setDialog :editProfile false])
                       }]
     ])
  )