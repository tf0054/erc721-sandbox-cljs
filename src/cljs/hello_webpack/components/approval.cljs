(ns hello-webpack.components.approval
  (:require [re-frame.core :as re-frame]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.subs :as subs]
            [hello-webpack.events :as e]
            [hello-webpack.ethereum.events :as ee]
            [hello-webpack.utils :as u]))

(defn view []
  (let [account (re-frame/subscribe [::subs/account])
        email (re-frame/subscribe [::subs/profile :email])]
    [:div
     [:div {:class "note"}
      "Give a permission to our system for enabling to deal with your timeslot-tokens."
      ]

     [ui/button {:children "Approve"
                 :variant  "contained"
                 :style    {:margin-bottom "5px"}
                 :on-click #(re-frame/dispatch [::ee/mytokenSetApprovalForAll])}]
     [:br]
     #_[:a {:href "/static/cal/index.html"} "CalendarPage"]
     [:hr]
     #_[ui/raised-button {:style   {:color "white" :text-weight "bold"}
                        :on-click #(re-frame/dispatch [::e/drawerToggle])} "menu"]
      ]
     )
    )
