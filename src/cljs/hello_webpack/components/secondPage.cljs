(ns hello-webpack.components.secondPage
  (:require [re-frame.core :as re-frame]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.subs :as subs]
            [hello-webpack.drizzle :as drizzle]
            [hello-webpack.utils :as u]
            [re-frame-datatable.core :as dt]
            [hello-webpack.reactable :as rtable]))

(defn view []
  (let [tokenSoldTableData (re-frame/subscribe [::subs/tokenSoldTableData])
        tokenSellerTableData (re-frame/subscribe [::subs/tokenSellerTableData])
        tokenBuyerTableData (re-frame/subscribe [::subs/tokenBuyerTableData])
        tokenPastTableData (re-frame/subscribe [::subs/tokenPastTableData])
        ]
    [:div
     [:div {:class "note"} "The table below isnt synced with chain (yet)"]
     ;;
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [ui/card-header {:title "Your timeslot(s) someone bought"}]
      (if (not (empty? @tokenSoldTableData))
        [:div [dt/datatable
               :datatable-sold
               [::subs/tokenSoldTableData]
               (hello-webpack.reactable/defSoldTableData)
               {::dt/pagination    {::dt/enabled? true
                                    ::dt/per-page 5}
                ::dt/table-classes ["ui" "table" "celled"]}]
         [rtable/gmail-like-pagination :datatable-sold [::subs/tokenSoldTableData]]]
        [:span "No timeslot(s) was found"])
      ]
     ;;
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [ui/card-header {:title "Your timeslot(s) on the market"}]
      (if (not (empty? @tokenSellerTableData))
        [:div [dt/datatable
               :datatable-seller
               [::subs/tokenSellerTableData]
               (hello-webpack.reactable/defSellerTableData)
               {::dt/pagination    {::dt/enabled? true
                                    ::dt/per-page 5}
                ::dt/table-classes ["ui" "table" "celled"]}]
         [rtable/gmail-like-pagination :datatable-seller [::subs/tokenSellerTableData]]]
        [:span "No timeslot(s) was found"])
      ]
     ;;
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [ui/card-header {:title "Timeslot(s) you have"}]
      (if (not (empty? @tokenBuyerTableData))
        [:div [dt/datatable
               :datatable-buyer
               [::subs/tokenBuyerTableData]
               (hello-webpack.reactable/defBuyerTableData)
               {::dt/pagination    {::dt/enabled? true
                                    ::dt/per-page 5}
                ::dt/table-classes ["ui" "table" "celled"]}]
         [rtable/gmail-like-pagination :datatable-buyer [::subs/tokenBuyerTableData]]]
        [:span "No timeslot(s) was found (as buyer)"])
      ]
     ;;
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [ui/card-header {:title "Timeslot(s) you had"}]
      (if (not (empty? @tokenPastTableData))
        [:div [dt/datatable
               :datatable-past
               [::subs/tokenPastTableData]
               (hello-webpack.reactable/defPastTableData)
               {::dt/pagination    {::dt/enabled? true
                                    ::dt/per-page 5}
                ::dt/table-classes ["ui" "table" "celled"]}]
         [rtable/gmail-like-pagination :datatable-past [::subs/tokenPastTableData]]]
        [:span "No past timeslot(s) was found (as buyer)"])
      ]
     #_[:div {:class "note"} "You can now create your meeting slots on your "
        [:a {:href   "https://calendar.google.com/calendar/"
             :target "_blank"} "Google calendar"] "."]
     ]
    )
    )
