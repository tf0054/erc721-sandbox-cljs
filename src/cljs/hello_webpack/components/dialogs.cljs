(ns hello-webpack.components.dialogs
  (:require [re-frame.core :as re-frame]
            [cljs-react-material-ui.reagent :as ui]
            [hello-webpack.events :as e]
            [hello-webpack.utils :as u]
            [hello-webpack.subs :as subs]
            [hello-webpack.components.dialog :as cd]))

(defn editProfile []
  (fn []
    (let [givenName (re-frame/subscribe [::subs/profile :givenName])
          familyName (re-frame/subscribe [::subs/profile :familyName])
          email (re-frame/subscribe [::subs/profile :email])
          linkedInButton (re-frame/subscribe [::subs/linkedIn :button])
          linkedInAuthUrl (re-frame/subscribe [::subs/linkedIn :url])
          linkedInUrl (re-frame/subscribe [::subs/profile :linkedInUrl])
          headline (re-frame/subscribe [::subs/profile :headline])
          summary (re-frame/subscribe [::subs/profile :summary])
          ]
      [ui/dialog-content {:style {:width      "450px"
                                  :height     "380px"
                                  :text-align "center"}}
       [:div
        (if (or (nil? @linkedInUrl) (empty? @linkedInUrl))
          [ui/button {:style    {:text-weight "bold"}
                      :disabled (not @linkedInButton)
                      :on-click #(.open js/window @linkedInAuthUrl
                                        "_blank"
                                        (str "toolbar=no,scrollbars=yes,resizable=yes,top=500,left=500,"
                                             "width=400,height=480"))}
           "LinkedIn"])
        [:br]
        [ui/text-field {:type        "text"
                        :placeholder "givenName"
                        :name        "edit-givenName"
                        :style       cd/dialog-text-width
                        :value       @givenName
                        :on-change   #(re-frame/dispatch [::e/setProfile :givenName (u/e-val %)])}]
        [:div
         [ui/text-field {:type        "text"
                         :placeholder "familyName"
                         :name        "edit-fivenName"
                         :style       cd/dialog-text-width
                         :value       @familyName
                         :on-change   #(re-frame/dispatch [::e/setProfile :familyName (u/e-val %)])}]]
        [:div
         [ui/text-field {:type        "text"
                         :placeholder "email"
                         :name        "edit-email"
                         :style       cd/dialog-text-width
                         :value       @email
                         :on-change   #(re-frame/dispatch [::e/setProfile :email (u/e-val %)])}]]
        [:div
         [ui/text-field {:type        "text"
                         :placeholder "linkedin"
                         :name        "edit-linkedin"
                         :style       cd/dialog-text-width
                         :value       @linkedInUrl
                         :on-change   #(re-frame/dispatch [::e/setProfile :linkedInUrl (u/e-val %)])}]]
        [:div
         [ui/text-field {:type        "text"
                         :placeholder "headline"
                         :name        "edit-headline"
                         :style       cd/dialog-text-width
                         :value       @headline
                         :on-change   #(re-frame/dispatch [::e/setProfile :headline (u/e-val %)])}]]
        [:div
         [ui/text-field {:type        "text"
                         :multiline   true
                         :rows        5
                         :placeholder "summary"
                         :name        "edit-summary"
                         :style       cd/dialog-text-width
                         :value       @summary
                         :on-change   #(re-frame/dispatch [::e/setProfile :summary (u/e-val %)])}]]]
       ])))

(defn metamaskRequest []
  (fn [x]
    (let [])
    [:div {:style {:text-align "center"}}
     [:div (case (u/checkMetamask)
             0 "Please install Metamask on your browser"
             1 "Please login to Metamask!")
      ]]
    ))