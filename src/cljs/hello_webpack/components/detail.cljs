(ns hello-webpack.components.detail
  (:require [re-frame.core :as re-frame]
            [reagent.core :as r]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            [hello-webpack.subs :as subs]
            [hello-webpack.events :as e]
            [hello-webpack.utils :as u]
            [hello-webpack.ethereum.events :as ee]
            [hello-webpack.components.dialog :as d]
            [hello-webpack.components.dialogs :as ds])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(defn view []
  (let [s (re-frame/subscribe [::subs/selectedCourse])
        f (:first @s)
        times (:times @s)
        p (re-frame/subscribe [::subs/profiles (:hostAddr f)])
        requestMetamask (re-frame/subscribe [::subs/dialog :requestMetamask])]
      [ui/card {:key (str (:descriptionId f) "_card")
                :style {:margin "40px 20px 20px 20px"}}
       [:div {:key (str (:descriptionId f) "_label")}
        "DETAIL PAGE:" ]
       [ui/card-header {:key (str (:descriptionId f) "_header")
                        :title (:title f)
                        :subheader "subheader is here"}]
       [ui/card-content {:key (str (:descriptionId f) "_body")}
        [:img {:key (str (:descriptionId f) "_img")
               :src (:imageUrl f)}]
        [ui/typography (str f)]
        (for [y times]
          [ui/card-content {:key (str (:descriptionId f) "_" (:objectID y))}
           [ui/typography {:key (str (:descriptionId f) "_" (:objectID y) "_txt")}
            (str y)]
           [:button {:key      (str (:descriptionId f) "_" (:objectID y) "_buy")
                     :id       (str "tokhimo-buy-" (:objectID y))
                     :on-click #(re-frame/dispatch [::ee/mytokenBuyToken
                                                    (:eventId y)
                                                    (:address f)
                                                    (:objectID y)
                                                    (:startTimeLong f)])}
            (str "buy (" (:objectID y) ")")]] )
        [ui/typography {:key (str (:descriptionId f) "_profile")}
         (if (nil? @p)
           (str {})
           (str @p))]
        [:hr]
        [ui/button {:children "back"
                    :variant  "contained"
                    :style    {:text-weight "bold" :margin-left "10px"}
                    :on-click #(re-frame/dispatch [::u/pageChange "search"])}]
        ]
       ;;
       [d/common-dialog {:title    "Require metamask"
                         :contents ds/metamaskRequest
                         :open     @requestMetamask
                         :ok-label "Done"
                         :ok       #(do
                                      (re-frame/dispatch [::ee/getLocalNetworkId])
                                      (re-frame/dispatch [::e/setDialog :requestMetamask false]))
                         :cancel   #(re-frame/dispatch [::e/setDialog :requestMetamask false])}
        ]
       ]
  ))