(ns hello-webpack.components.dialog
  (:require [reagent.core :as r]
            [cljsjs.material-ui]
            [cljs-react-material-ui.core :refer [get-mui-theme color]]
            [cljs-react-material-ui.reagent :as ui]
            [cljs-react-material-ui.icons :as ic]
            ))

(def dialog-text-width {:width "90%"})

(defn common-dialog
  [{open :open ok :ok cancel :cancel title :title contents :contents ok-label :ok-label style :style}]
  [ui/dialog {:title            title
              ;:primary          true
              ;:modal            false
              :disable-escape-key-down false
                                ;:contentStyle     (if (nil? style)
                                ;  {:max-width 400}
                                ;  style)
              :open             open
              :on-request-close cancel}
   [contents]
   [ui/dialog-actions
    (if cancel
      (r/as-element [ui/button {:children "Cancel" :variant "outlined" :color "primary" :on-click cancel}]))
     (r/as-element [ui/button {:children ok-label :variant "outlined" :color "primary" :on-click ok}])
    ]
   ])