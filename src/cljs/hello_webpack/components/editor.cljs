(ns hello-webpack.components.editor
  (:require
    [cljsjs.quill]
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [hello-webpack.subs :as subs]
    [hello-webpack.events :as e]
    [hello-webpack.ajax :as ajax]
    [hello-webpack.components.quill :as quill]
    [cljs-react-material-ui.reagent :as ui]
    [re-frame-datatable.core :as dt]
    [hello-webpack.utils :as u])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(defn view []
  (let [address (re-frame/subscribe [::subs/account])
        contentNew (re-frame/subscribe [::subs/contents :new])
        contentsTable (re-frame/subscribe [::subs/contents :lists])]
    [:div
     [ui/card {:style {:height "100%"
                       :margin "40px 20px 60px 20px"}}
      [ui/card-header {;:key (str (:descriptionId f) "_header")
                       :title "Create contents here."
                       :subheader "subheader is here"}]
      [quill/editor
       {:id           "tokhimo-editor"
        :content      @contentNew
        :selection    nil
        :on-change-fn #(if (= % "user")
                         (re-frame/dispatch [::e/setTmpQuillContent %2]))}]]
     [:hr]
     [ui/button {:children "save"
                 :variant  "contained"
                 :disabled (if (nil? @address) true false)
                 ;:color "secondary"
                 :style    {:text-weight "bold" :margin-left "10px"}
                 :on-click #(re-frame/dispatch [::ajax/setQuillContent])}]
     [:hr]
     [ui/card {:style {:margin "40px 20px 20px 20px"}}
      [:div "Content(s)"]
      [:div
       (if (not (empty? @contentsTable))
         ;;
         [dt/datatable
          :datatable-contents
          [::subs/contents :lists]
          (hello-webpack.reactable/defContentsTableData)
          {::dt/pagination    {::dt/enabled? true
                               ::dt/per-page 5}
           ::dt/table-classes ["ui" "table" "celled"]}]
         ;;
         [:span "No prepared contents for slots."])
       ]]
     ]
    )
  )
