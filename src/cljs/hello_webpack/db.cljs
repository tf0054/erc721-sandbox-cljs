(ns hello-webpack.db)

(def default-db
  {:page                "googleLogin"
   :drawer              false
   :firstSync           false
   :profile             {:id          nil
                         :email       nil
                         :givenName   nil
                         :familyName  nil
                         :imageUrl    nil
                         :linkedInUrl ""
                         :headline    ""
                         :summary     ""
                         :address     nil
                         :network     nil
                         :approved    false
                         :metamask    false}
   :profiles            {}
   :serverProfile       {:network -1
                         :address nil
                         :balance 0}
   :stashedPurchase     []
   :selectedCourse      {}
   :drizzleStore        nil
   :drizzleContracts    nil
   :tokenId             ""
   :tokenMsg            ""
   :allMsgs             nil
   :events              {}
   :tokenProfile        {:address nil
                         :balance 0}
   ; :tokenSoldTableData   []
   ; :tokenSellerTableData []
   ; :tokenBuyerTableData  []
   ; :tokenPastTableData   []
   :tokenDebugTableData []
   :search              {:word  ""
                         :index nil
                         :cards nil}
   :contents            {:count 0
                         :new   "Tokhimo editor!"
                         :lists []
                         :cards '()}
   :linkedIn            {:url    ""
                         :button true}
   :dialog              {:editProfile     false
                         :requestMetamask false}
   :tmp                 {}
   })
