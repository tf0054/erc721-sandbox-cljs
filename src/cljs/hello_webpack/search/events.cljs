(ns hello-webpack.search.events
  (:require [re-frame.core :as re-frame]
            [hello-webpack.reactable :as rtable]
            [hello-webpack.ajax :as ajax]
            [hello-webpack.utils :as u]
            [cljs-time.format :as tf])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(re-frame/reg-event-db                                      ;; note the -fx
  ::setSearchIndex
  (fn [db [_ _]]
    (let [indexName (clojure.string/lower-case (u/getTokenAddr db))
          index (.initIndex js/algoliaclient indexName)]
      (.setSettings index (clj->js {:attributesForFaceting ["descriptionId","searchable(descriptionId)"]}))

      (log "Algolia search index is" indexName)
      (assoc-in db [:search :index] index))
    ))

(defn searchCardsDone [err content]
  (if (empty? err)
    (let [result (js->clj content :keywordize-keys true)
          format (tf/formatters :date-time)
          data (remove nil?
                       (doall (map #(-> %
                                        (assoc-in [:startTime] (tf/parse format (get-in % [:startTime])))
                                        (assoc-in [:endTime] (tf/parse format (get-in % [:startTime]))))
                                   (:hits result))))
          ]
      (re-frame/dispatch [::ajax/loadProfile (:hostAddr (first data))])
      (log (str "algolia(result):\n" result))
      (re-frame/dispatch [::rtable/mkSearchCardsData data]))
    (do (log (str "algolia(err):" err)))
    )
  )

(re-frame/reg-event-db
  ::getSearchCardsResult
  (fn [db [_ word facetKey]]
    (let [userAddr (u/addr db)
          filters (str "startTimeLong > " (js/parseInt (/ (js/Date.)  1000)))]
    (.search (get-in db [:search :index] db)
                           (clj->js {:query        word
                                     :filters      (if (nil? userAddr)
                                                     filters
                                                     (str filters " AND NOT hostAddr:" userAddr))
                                     :facetFilters [(str "descriptionId:" facetKey)]
                                     })
                           searchCardsDone) )
    db))

(re-frame/reg-event-db
  ::getSearchCardsFacets
  (fn [db [_ word]]
    (log (str ::getSearchcardsResult " " word))
    (if-let [index (get-in db [:search :index] db)]
      (.searchForFacetValues index
                             (clj->js {:facetQuery "*"
                                       :facetName  "descriptionId"
                                       })
                             (fn [err content]
                               (if (empty? err)
                                 (let [result (js->clj content :keywordize-keys true)]
                                   (doall (for [facet (:facetHits result)]
                                            (do (re-frame/dispatch [::getSearchCardsResult word (:value facet)])
                                                (log (str "algolia(result):\n" (:value facet) word)))
                                            )))
                                 (log (str "algolia(err):" err)) )
                               ))
      (log "No index was initiated.") )
    db))

(re-frame/reg-event-db
  ::setSearchWord
  (fn [db [_ partialWord]]
    (if (not (empty? partialWord))
      (re-frame/dispatch [::getSearchCardsFacets partialWord]))
    (-> db
        (assoc-in [:search :cards] [])
        (assoc-in [:search :word] partialWord))
    ))