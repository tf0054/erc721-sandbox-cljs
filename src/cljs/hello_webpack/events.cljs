(ns hello-webpack.events
  (:require
    [reagent.core :as r]
    [re-frame.core :as re-frame]
    [day8.re-frame.async-flow-fx]
    [hello-webpack.db :as db]
    [hello-webpack.store :as s]
    [hello-webpack.utils :as u]
    [hello-webpack.ajax :as ajax]
    [hello-webpack.drizzle :as drizzle]
    [hello-webpack.ethereum.events :as ee]
    [hello-webpack.search.events :as se]
    [hello-webpack.linkedin.events :as le]
    ;[day8.re-frame.tracing :refer-macros [fn-traced defn-traced]]
    )
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(def interceptors [#_(when ^boolean js/goog.DEBUG debug)
                   re-frame/trim-v])

(defn init-flow []
  (log "entering init-flow")
  {;; ::setGoogleProfile was kicked by index.html
   :rules [{:when       :seen? :events [::setGoogleProfile]
            :dispatch-n '([::ajax/loadProfile] [::ajax/getServerInfo] [::le/getLinkedInUrl])}
           {:when       :seen? :events [::ajax/loadProfileSuccess ::ajax/getServerInfoSuccess]
            :dispatch-n '([::checkLinkedIn] [::se/setSearchIndex])}
           ;; OR conditions
           {:when       :seen? :events [::ajax/saveProfileSuccess]
            :dispatch-n '([::boot] [::boot-level-log "init"])
            :halt?      true}
           {:when       :seen? :events [::doneCheckLinkedIn]
            :dispatch-n '([::boot] [::boot-level-log "init"])
            :halt?      true}]
   }
  )

(re-frame/reg-event-db
  ::checkLinkedIn
  (fn [db [_ _]]
    (log (str ::checkLinkedIn))
    (if (not (empty? (get-in db [:profile :headline])))
      (do (re-frame/dispatch [::doneCheckLinkedIn])
          (re-frame/dispatch [::u/pageChange "booting"]))
      (re-frame/dispatch [::setDialog :editProfile true]) )
    db)
  )

(re-frame/reg-event-db
  ::doneCheckLinkedIn
  (fn [db [_ _]]
    db)
  )

(defn boot-flow []
  {;; what event kicks things off ?
   ; :first-dispatch [::noop]
   ;; a set of rules describing the required flow
   :rules [#_{:when       :seen? :events [::drizzle/drizzle-started]
              :dispatch-n '([::ajax/getServerInfo] #_[::drizzle/getTokenAddr])}
           ;; 1
           {:when       :seen? :events [::drizzle/drizzle-started
                                        ]
            :dispatch-n '([::ee/getServerBalance] [::ee/getTokenBalance] [::boot-level-log 1])}
           ;; 2
           {:when       :seen-all-of? :events [::ee/setServerBalance ::ee/setTokenBalance
                                               ]
            :dispatch-n '([::ee/getLocalNetworkId] [::ee/mytokenGetTotal] [::boot-level-log 2])}
           ;; 3
           {:when       :seen-all-of? :events [::ee/setLocalInfo
                                               ::drizzle/doneGetTotalSupply]
            :dispatch-n '([::drizzle/updateOwnerByAll]
                           ;[::ee/mytokenIsApprovedForAll]
                           [::ee/checkMetamask]
                           [::boot-level-log 3])} ;; needed ::ee/setLocalNetworkId first
           ;; 4
           {:when       :seen-all-of? :events [::drizzle/doneUpdateOwnerByAll]
            :dispatch-n '([::updateProfiles] [::boot-level-log 4])}
           ;;
           {:when       :seen-all-of? :events [::drizzle/doneUpdateOwnerByAll
                                               ; ::drizzle/setApprovedForAll
                                               ]
            :dispatch-n '([::ajax/saveProfile]
                           [::u/pageChange "second"] [::boot-finished])
            :halt?      true}]
   })

(re-frame/reg-event-db
  ::boot-level-log
  (fn [db [_ x]]
    (log (str "boot-flow level: " x " is finished."))
    db))

(re-frame/reg-event-fx           ;; note the -fx
  ::init
  (fn [_ _]
    {;; do whatever synchronous work needs to be done like set state to show "loading" twirly for user??
     :db (-> db/default-db)
     :async-flow (init-flow) ;; kick off the async process
     }))

(re-frame/reg-event-fx           ;; note the -fx
  ::boot                          ;; usage:  (dispatch [:boot])  See step 3
  (fn [{:keys [db]} _]
    (if (= 2 (u/checkMetamask)) ;LoggedIn
      (do (log (str ::boot "\nboot-flow started."))
          (drizzle/setDrizzleStoreListener)
          {;; do whatever synchronous work needs to be done like set state to show "loading" twirly for user??
           :db         db
           :async-flow (boot-flow)})
      (do
        (re-frame/dispatch [::se/setSearchIndex])
        (re-frame/dispatch [::u/pageChange "search"]))
      )
    ))

(re-frame/reg-event-db
  ::boot-finished
  (fn [db [_ _]]
    (log "boot-flow was finished.")
    db))

(re-frame/reg-event-db
  ::setTokenId
  (fn [db [_ id]]
    (assoc-in db [:tokenId] id)
    ))

(re-frame/reg-event-db
  ::setTokenMsg
  (fn [db [_ msg]]
    (assoc-in db [:tokenMsg] msg)
    ))

(re-frame/reg-event-db
  ::updateProfiles
  (fn [db [_ _]]
    (let [tokenHashes (u/tokenHashFromSortedOwnerOf db)
          numOfTokenHashes (count tokenHashes)
          ownersToLoad (remove #{(get-in db [:serverProfile :address])
                                 (get-in db [:profile :address])}
                              (distinct (map #(u/ownerOfByTokenHash db %) tokenHashes)))]
      (log (str ::updateProfiles " " (count ownersToLoad) " " numOfTokenHashes))
      (if (> numOfTokenHashes 0)
        (do
          (doall (map (fn [addr]
                        (re-frame/dispatch [::ajax/loadProfile addr]))
                      ownersToLoad))
          ; TODO
          (re-frame/dispatch [::drizzle/updateAll true]))
        (if (and (not (nil? (:drizzleContracts db)))
                 (not= 0 (u/getTokenTotal db)))
          (u/wait #(re-frame/dispatch [::updateProfiles]) 500)
          (log (str ::updateProfiles " Nothing done." ))
          ) )
      db)
    ))

;---
(re-frame/reg-event-db
  ::drawerToggle
  (fn [db [_]]
    #_(log (str "db: " (:drawer db)))
    (assoc db :drawer (not (:drawer db)))
    ))

;(re-frame/reg-event-db
;  ::setTokenMeta
;  (fn [db [_ attr msg]]
;    (assoc-in db [:tokenJsonBase (keyword attr)] msg)
;    ))

;---
(re-frame/reg-event-db
  ::setGoogleProfile
  (fn [db [_ x]]
    (let [p (js->clj x :keywordize-keys true)]
      (log (str ::setGoogleProfile "\n" p "->" (:profile db)))
      (re-frame/dispatch [::u/pageChange "search"])
      (-> db
          (assoc :profile (merge (:profile db) p))))
    ))

(re-frame/reg-event-db
  ::setProfile
  (fn [db [_ x value]]
    #_(log (str ::setProfile "\n" x " " value))
    (-> db
        (assoc-in [:profile x] value))
    ))

(re-frame/reg-event-db
  ::setTmpQuillContent
  (fn [db [_ content]]
    #_(log (str ::setTmpQuillContent "\n" content))
    ;(re-frame/dispatch [::ajax/getContentsInfo])
    (assoc-in db [:contents :new] content)
    ))

(re-frame/reg-event-db
  ::setDialog
  (fn [db [_ target status]]
    (assoc-in db [:dialog target] status)
    ))

(re-frame/reg-event-db
  ::setSelectedCourse
  (fn [db [_ f times]]
    (assoc-in db [:selectedCourse] {:first f :times times})
    ))