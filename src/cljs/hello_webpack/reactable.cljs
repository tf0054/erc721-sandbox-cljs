(ns hello-webpack.reactable
  (:require [re-frame.core :as re-frame]
            [re-frame-datatable.core :as dt]
            #_[hello-webpack.subs :as subs]
            [hello-webpack.utils :as u]
            [hello-webpack.ethereum.utils :as eu]
            [hello-webpack.ethereum.events :as ee]
            [cljs-time.core :as tc])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(defn convToProfile [db x]
      (if-let [p (u/getProfiles db x)]
        [:div [:img {:src (:imageUrl p)}]
              [:span (str (:givenName p) " " (:familyName p))]]
        [:span {:style {:font-family "monospace"}} x]
        )
  )

(re-frame/reg-event-db
::mkSearchCardsData
(fn [db [_ content]]
  (let [data (if (empty? (get-in db [:search :cards]))
               (cons content '())
               (concat (get-in db [:search :cards]) (cons content '())))]
    #_(log (str ::mkSearchCardsData "\n" data))
    (assoc-in db [:search :cards] data))
  ))

; TODO
(defn eventId [db tId] (get-in db [:events (keyword (str tId)) :id]))
(defn eventTime [db tId startend] (get-in db [:events (keyword (str tId)) startend :dateTime]))
(defn eventTitle [db tId] (get-in db [:events (keyword (str tId)) :summary]))
(defn eventHtmlLink [db tId] (get-in db [:events (keyword (str tId)) :htmlLink]))

(defn defSoldTableData []
  [{::dt/column-key   [:tokenId]
    ::dt/sorting      {::dt/enabled? true}
    ::dt/column-label "#"}
   {::dt/column-key   [:start]
    ::dt/column-label "start"}
   {::dt/column-key   [:end]
    ::dt/column-label "end"}
   {::dt/column-key   [:title]
    ::dt/column-label "title"}
   {::dt/column-key   [:buyer]
      ::dt/column-label "buyer"}
   #_{::dt/column-key   [:seller]
    ::dt/column-label "seller"}
   {::dt/column-key   [:price]
    ::dt/column-label "price"}]
  )

(defn defSellerTableData []
  [{::dt/column-key   [:tokenId]
    ::dt/sorting      {::dt/enabled? true}
    ::dt/column-label "#"}
   {::dt/column-key   [:start]
    ::dt/column-label "start"}
   {::dt/column-key   [:end]
    ::dt/column-label "end"}
   {::dt/column-key   [:title]
    ::dt/column-label "title"}
   #_{::dt/column-key   [:buyer]
      ::dt/column-label "buyer"}
   #_{::dt/column-key   [:seller]
    ::dt/column-label "seller"}
   {::dt/column-key   [:price]
    ::dt/column-label "price"}]
  )

(defn defBuyerTableData []
  [{::dt/column-key   [:tokenId]
    ::dt/sorting      {::dt/enabled? true}
    ::dt/column-label "#"}
   {::dt/column-key   [:start]
    ::dt/column-label "start"}
   {::dt/column-key   [:end]
    ::dt/column-label "end"}
   {::dt/column-key   [:title]
    ::dt/column-label "title"}
   #_{::dt/column-key   [:buyer]
      ::dt/column-label "buyer"}
   {::dt/column-key   [:seller]
    ::dt/column-label "seller"}
   {::dt/column-key   [:price]
    ::dt/column-label "price"}]
  )

(defn defPastTableData []
  [{::dt/column-key   [:tokenId]
    ::dt/sorting      {::dt/enabled? true}
    ::dt/column-label "#"}
   {::dt/column-key   [:start]
    ::dt/column-label "start"}
   {::dt/column-key   [:end]
    ::dt/column-label "end"}
   {::dt/column-key   [:title]
    ::dt/column-label "title"}
   #_{::dt/column-key   [:buyer]
    ::dt/column-label "buyer"}
   {::dt/column-key   [:seller]
    ::dt/column-label "seller"}
   {::dt/column-key   [:price]
    ::dt/column-label "price"}
   {::dt/column-key   [:confirm]
    ::dt/column-label "confirm"}]
  )

(def myPageTableCache (atom {}))

(defn mkMyPageTableData [db]
  (sort-by :tokenId
           (map #(let [tId (u/tokenIdByTokenHash db %)
                       endTime (eventTime db tId :end)
                       owner (= (u/addr db) (u/ownerOfByTokenHash db %))
                       host (= (u/addr db) (u/getHostByTokenHash db %))
                       burn (= (get-in db [:serverProfile :address])
                               (u/ownerOfByTokenHash db %))
                       past (< endTime (tc/now))
                       priceKey (keyword (str "price_" tId))
                       ;;
                       price (re-frame/subscribe [::u/getTokenPriceCP tId])]
                   (if (nil? (get-in db [:tmp priceKey]))
                     (re-frame/dispatch [::ee/copyTokenPriceToTmp tId]))
                   (-> {:tokenId tId #_[:a {:href   (str "https://rinkeby.opensea.io/assets/"
                                                         (:tokenAddr db) "/"
                                                         tId)
                                            :target "_blank"}
                                        tId]
                        :start   [:a {:href   (eventHtmlLink db tId)
                                      :target "_blank"}
                                  (str (eventTime db tId :start))]
                        :end     (str endTime)
                        :title   (eventTitle db tId)
                        :buyer   (convToProfile db (u/ownerOfByTokenHash db %))
                        :seller  (convToProfile db (u/getHostByTokenHash db %))
                        :price   (if (and owner host (not past))
                                   [:div [:input {:type      "text"
                                                  :name      "price"
                                                  ;:style "width: 80%;"
                                                  :on-change (fn [evt]
                                                               (re-frame/dispatch [::u/setTmp priceKey
                                                                                   (u/e-val evt)]))
                                                  :value     @price}]
                                    [:input {:type     "submit"
                                             ;:style   "position: absolute"
                                             :value    "set"
                                             :on-click (fn [_]
                                                         (re-frame/dispatch [::ee/mytokenSetPrice
                                                                             (get-in db [:tmp priceKey])
                                                                             tId]))}]]
                                   (str (eu/from-wei (u/getPriceByTokenHash db %))))
                        ;;
                        :confirm (if owner
                                   [:button {:on-click (fn []
                                                         (re-frame/dispatch [::ee/mytokenSetReviewed tId]))}
                                    (str "Finished (" tId ")")]
                                   )
                        :burn?   burn
                        :host?   host
                        :owned?  owner
                        :past?   past
                        }
                       ))
                (u/tokenHashFromSortedOwnerOf db)))
  )

(defn convTableData [category _data]
  (log (str "update " category " table.." (count @myPageTableCache)))
  (let [data (if (nil? _data)
               @myPageTableCache
               (do (reset! myPageTableCache _data)
                   _data))]
    (remove nil?
            (case category
              :sold (map #(if (not (:burn? %))
                            (if (and (not (:past? %))
                                     (:host? %)
                                     (not (:owned? %)))
                              (dissoc % :past? :seller :confirm :burn? :owned? :host?)))
                         data)
              :buyer (map #(if (not (:burn? %))
                             (if (and (not (:past? %))
                                      (not (:host? %))
                                      (:owned? %))
                               (dissoc % :past? :buyer :confirm :burn? :owned? :host?)))
                          data)
              :seller (map #(if (not (:burn? %))
                              (if (and (not (:past? %))
                                       (:host? %)
                                       (:owned? %))
                                (dissoc % :past? :seller :buyer :confirm :burn? :owned? :host?)))
                           data)
              :past (map #(if (and (not (:burn? %))
                                   (:past? %))
                            (if (or (:host? %)
                                    (:owned? %))
                              (if (:host? %)
                                (dissoc % :past? :buyer :confirm :burn? :owned? :host?)
                                (dissoc % :past? :buyer :burn? :owned? :host?) )
                              ))
                         data)
              ))
  ))

(defn defDebugTableData []
  [{::dt/column-key   [:tokenId]
    ::dt/sorting      {::dt/enabled? true}
    ::dt/column-label "#"}
   {::dt/column-key   [:start]
    ::dt/column-label "start"}
   {::dt/column-key   [:end]
    ::dt/column-label "end"}
   {::dt/column-key   [:price]
    ::dt/column-label "price"}
   {::dt/column-key   [:owner]
    ::dt/column-label "owner"}
   {::dt/column-key   [:host]
    ::dt/column-label "host"}
   {::dt/column-key   [:tokenHash]
    ::dt/column-label "tokenHash"}
   {::dt/column-key   [:past?]
    ::dt/column-label "past?"}
   {::dt/column-key   [:burn?]
    ::dt/column-label "burn?"}
   {::dt/column-key   [:owner?]
    ::dt/column-label "owner?"}
   {::dt/column-key   [:host?]
    ::dt/column-label "host?"}
   #_{::dt/column-key   [:duration]
      ::dt/column-label "Duration"
      ::dt/sorting      {::dt/enabled? true}
      ::dt/render-fn    (fn [val]
                          [:span
                           (let [m (quot val 60)
                                 s (mod val 60)]
                             (if (zero? m)
                               s
                               (str m ":" (when (< s 10) 0) s)))])}]
  )

(re-frame/reg-event-db
  ::mkDebugTableData
  (fn [db [_]]
    (let [data (sort-by :sortId
                        (map #(let [tId (u/tokenIdByTokenHash db %)
                                    endTime (eventTime db tId :end)
                                    owner (= (u/addr db) (u/ownerOfByTokenHash db %))
                                    host (= (u/addr db) (u/getHostByTokenHash db %))
                                    burn (= (get-in db [:serverProfile :address])
                                            (u/ownerOfByTokenHash db %))
                                    past (< endTime (tc/now))
                                    ]
                                (-> {:sortId tId
                                     :tokenId   [:a {:href   (str "https://rinkeby.opensea.io/assets/"
                                                                                (u/getTokenAddr db) "/"
                                                                                tId)
                                                                   :target "_blank"}
                                                               tId]
                                     :start     [:a {:href   (eventHtmlLink db tId)
                                                     :target "_blank"}
                                                 (str (eventTime db tId :start))]
                                     :end       (str endTime)
                                     :price     (str (eu/from-wei (u/getPriceByTokenHash db %)))
                                     :owner     (if (not= (u/ownerOfByTokenHash db %)
                                                          (get-in db [:serverProfile :address]))
                                                  [:a {:href   (str "https://rinkeby.etherscan.io/address/"
                                                                    (u/ownerOfByTokenHash db %))
                                                       :target "_blank"}
                                                   (if (not (nil? (u/ownerOfByTokenHash db %)))
                                                     (u/shortenStr (u/ownerOfByTokenHash db %))
                                                     "-")]
                                                  "burned")
                                     :tokenHash [:a {:href   (str "https://rinkeby.opensea.io/assets/"
                                                                   (u/getTokenAddr db) "/"
                                                                   tId)
                                                      :target "_blank"}
                                                  (str (subs (name %) 0 17) "..")]
                                     :tokenURI  [:a {:href   (u/tokenURIByTokenHash db %)
                                                     :target "_blank"}
                                                 (u/tokenURIByTokenHash db %)]
                                     :ignoreDue [:a {:href   (u/tokenURIByTokenHash db %)
                                                     :target "_blank"}
                                                 (u/tokenURIByTokenHash db %)]
                                     :host      (u/shortenStr (u/getHostByTokenHash db %))
                                     :burn?     (if burn "x" "")
                                     :host?     (if host "x" "")
                                     :owned?    (if owner "x" "")
                                     :past?     (if past "x" "")
                                     }))
                             (u/tokenHashFromSortedOwnerOf db)))]
      #_(log (str data))
      (-> db
          (assoc-in [:tokenDebugTableData] data))
      )
    ))

(defn defContentsTableData []
  [{::dt/column-key   [:id]
    ::dt/sorting      {::dt/enabled? true}
    ::dt/column-label "#"}
   {::dt/column-key   [:content]
    ::dt/column-label "start"}
   {::dt/column-key   [:created]
    ::dt/column-label "start"}
   {::dt/column-key   [:operation]
    ::dt/column-label "start"}]
  )

(defn gmail-like-pagination [db-id data-sub]
  (let [pagination-state (re-frame/subscribe [::re-frame-datatable.core/pagination-state db-id data-sub])]
    (fn []
      (let [{:keys [::re-frame-datatable.core/cur-page ::re-frame-datatable.core/pages]} @pagination-state
            total-pages (count pages)
            next-enabled? (< cur-page (dec total-pages))
            prev-enabled? (pos? cur-page)]

        [:div
         [:div {:style {:display      "inline-block"
                        :margin-right ".5em"}}
          [:strong
           (str (inc (first (get pages cur-page))) "-" (inc (second (get pages cur-page))))
           ]
          [:span " of "]
          [:strong (inc (second (last pages)))]]

         [:div.ui.pagination.mini.menu
          [:a.item
           {:on-click #(when prev-enabled?
                         (re-frame/dispatch [::re-frame-datatable.core/select-prev-page db-id @pagination-state]))
            :class    (when-not prev-enabled? "disabled")}
           [:i.left.chevron.icon]]

          [:a.item
           {:on-click #(when next-enabled?
                         (re-frame/dispatch [::re-frame-datatable.core/select-next-page db-id @pagination-state]))
            :class    (when-not next-enabled? "disabled")}
           [:i.right.chevron.icon]]]]))))