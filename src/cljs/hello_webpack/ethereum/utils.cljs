(ns hello-webpack.ethereum.utils)

(defonce nullAddr "0x0000000000000000000000000000000000000000")

(defn from-wei [wei]
  (/ wei 1000000000000000000))

(defn to-wei [eth]
  (* eth 1000000000000000000))