(ns hello-webpack.ethereum.events
  (:require
    [re-frame.core :as re-frame]
    [day8.re-frame.http-fx]
    [day8.re-frame.async-flow-fx]
    [hello-webpack.utils :as u]
    [hello-webpack.ethereum.utils :as eu]
    #_[cljs.reader :as reader]
    [ajax.core :as ajax])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(def txObj (atom {}))

(defn txAndStatus [newStore]
  (if (not= 0 (count (:transactionStack newStore)))
    (do (let [txHash (nth (:transactionStack newStore) (:stackId @txObj))]
          (str txHash ":" (:operation @txObj) " "
               (-> (:transactions newStore)
                   (get (keyword txHash))
                   :status))))))

(defn updateTxObj [stackId op]
  (swap! txObj assoc :stackId stackId)
  (swap! txObj assoc :operation op)
  )

(re-frame/reg-event-db
  ::mytokenMint
  (fn [db _]
    (let [stackId (.cacheSend (.-mint (u/getMyTokenMethods)))]
     (updateTxObj stackId "Mint")
      db)
    ))

(re-frame/reg-event-db
  ::mytokenSetPrice
  (fn [db [_ price tokenId]]
    #_(log (u/getMyTokenMethods))
    (log (str "p:" price " tokenId:" tokenId))
    (let [wei (eu/to-wei price)
          stackId (.cacheSend (.-setPrice (u/getMyTokenMethods)) tokenId wei)]
      (log (str ::mytokenSetPrice " "  wei))
      (updateTxObj stackId "setPrice")
      db)
    db))

(defn cbMyTokenBuyToken [params e txHash]
  (if (nil? e)
    (do (log (str ::mytokenBuyToken
                  " (cb) TxHash: " txHash " " (:tokenId params) " "
                  (:fromAddr params) " -> " (:toAddr params)))
        (re-frame/dispatch [::mytokenMoveToken (merge params {:txHash txHash})]))
    (log (str ::mytokenBuyToken " (cb) err: " e)))
  )

(re-frame/reg-event-db
  ::mytokenBuyToken
  (fn [db [_ eventId fromAddr tokenId startEpocSec]]
    (log (str ::mytokenBuyToken " " eventId ", " fromAddr ", " tokenId ", " startEpocSec))
    (if (< 0 (get-in db [:profile :metamask]))
      (if true #_(get-in db [:profile :approved])
        (let [buyContractFuncObj (.buyToken (u/getMyTokenMethods) tokenId startEpocSec)]
          (let [userAddr (u/addr db)
                tokenPrices (u/getPrices db)
                tokenPrice (u/getPriceByTokenHash db (nth (keys tokenPrices) tokenId))
                promise (.send buyContractFuncObj (clj->js {:from  userAddr
                                                            :value (str tokenPrice)
                                                            :gas   222222})
                               (partial cbMyTokenBuyToken {:eventId  eventId
                                                           :fromAddr fromAddr
                                                           :toAddr   userAddr
                                                           :tokenId  tokenId})
                               )]
            (log (str ::mytokenBuyToken " " eventId "(" tokenId ")\n"
                      fromAddr " -> " userAddr ", " tokenPrice " (" (eu/from-wei tokenPrice) ")"))
            db))
        (do (re-frame/dispatch [::mytokenIsApprovedForAll])
            (assoc-in db [:stashedPurchase] [eventId fromAddr tokenId]))
        )
      (do (re-frame/dispatch [::setDialog :requestMetamask true])
          (assoc-in db [:stashedPurchase] [eventId fromAddr tokenId]))
      )
  ))

(re-frame/reg-event-db
  ::moveTokenSuccess
  (fn [db [_ msg]]
    (log (str ::moveTokenSuccess " moteToken/txHash: " msg))
    db))
;---2
(re-frame/reg-event-fx
  ::mytokenMoveToken
  (fn [{:keys [db]} [_ data]]
      (log (str ::mytokenMoveToke " " data))
      {:db         (assoc db :show-twirly true)   ;; causes the twirly-waiting-dialog to show??
       :http-xhrio {:method          :post
                    :uri             "/moveToken"   ;; moving Token has to be called from server(clj/web3j)
                    :timeout         30000   ;; optional see API docs
                    :params          data
                    :format          (ajax/json-request-format)
                    :response-format (ajax/json-response-format {:keywords? true})  ;; IMPORTANT!: You must provide this.
                    :on-success      [::moveTokenSuccess]
                    :on-failure      [::u/apiAccessFailed]}}
    ))
;---

(re-frame/reg-event-db
  ::mytokenSetApprovalForAll
  (fn [db _]
    (let [stackId (.cacheSend (.-setApprovalForAll (u/getMyTokenMethods)) (get-in db [:serverProfile :address]) true)]
      (updateTxObj stackId "approvalForAll")
      (re-frame/dispatch [::u/pageChange "second"])
      db)
    ))

(re-frame/reg-event-db
  ::mytokenSetUrlByIndex
  (fn [db [_ id url]]
    (let [stackId (.cacheSend (.-setTokenURI (u/getMyTokenMethods)) id url)]
      (updateTxObj stackId "setUrl")
      db)
    ))

(re-frame/reg-event-db
  ::mytokenSetReviewed
  (fn [db [_ id]]
    (let [stackId (.cacheSend (.-setReviewed (u/getMyTokenMethods)) id)]
      (updateTxObj stackId "setReviewed")
      db)
    ))

(re-frame/reg-event-db
  ::mytokenGetTotal
  (fn [db _]
    (let [datakey (.cacheCall (.-totalSupply (u/getMyTokenMethods)))]
      db)
    ))

(re-frame/reg-event-db
  ::mytokenGetUrlByIndex
  (fn [db [_ id]]
    (let [datakey (.cacheCall (.-tokenURI (u/getMyTokenMethods)) id)]
      #_(prn "byIndex: " id datakey)
      db)
    ))

(re-frame/reg-event-db
  ::mytokenCheckPaid
  (fn [db [_ id]]
    (let [datakey (.cacheCall (.-checkPaid (u/getMyTokenMethods)) id)]
      (prn "checkStatus: " id datakey)
      db)
    ))

(re-frame/reg-event-db
  ::mytokenGetStartEpocSec
  (fn [db [_ id]]
    (let [datakey (.cacheCall (.-getStartEpocSec (u/getMyTokenMethods)) id)]
      (prn "getStartEpocSec: " id datakey)
      db)
    ))

(defn getTokenOwnerOfByIndex [id]
  (try
    (let [datakey (.cacheCall (.-ownerOf (u/getMyTokenMethods)) id)])
    (catch js/Error e
      (log (str "ownerOf: " e))))
  )

(re-frame/reg-event-db
  ::mytokenOwnerOfByIndex
  (fn [db [_ id]]
    (getTokenOwnerOfByIndex id)
    db)
  )

(re-frame/reg-event-db
  ::mytokenGetPrice
  (fn [db [_ id]]
    (let [datakey (.cacheCall (.-getPrice (u/getMyTokenMethods)) id)]
      db)
    ))

(re-frame/reg-event-db
  ::mytokenGetHost
  (fn [db [_ id]]
    (let [datakey (.cacheCall (.-getHost (u/getMyTokenMethods)) id)]
      db)
    ))

(re-frame/reg-event-db
  ::mytokenIsApprovedForAll
  (fn [db [_ _]]
    (let [userAddr (u/addr db)
          userEmail (get-in db [:profile :email])
          serverAddr (get-in db [:serverProfile :address])]
      (do (log (str ::mytokenIsApprovedForAll "\nu:" userAddr " s:" serverAddr))
          (if (empty? userAddr)
            (if (nil? userEmail)
              (re-frame/dispatch [::u/pageChange "googleLogin"])
              (re-frame/dispatch [::setDialog :requestMetamask true]))
            (do (log (str ::mytokenIsApprovedForAll " contract method was called."))
                (.cacheCall (.-isApprovedForAll (u/getMyTokenMethods))
                        userAddr
                        serverAddr))))
      ) ;; Response will be caught via Drizzle.
    db)
    )

(re-frame/reg-event-db
  ::setLocalInfo
  (fn [db [_ v]]
    (log (str "Metamask_netId: " v " " (if (= v (get-in db [:serverProfile :network]))
                                         "matched!"
                                         "not matched..")
              " " (if (> (count (keys (:profile db))) 4)
                    "profile sufficiency OK"
                    "profile sufficiency NG")))
    ;TODO user's address should be get directly from Metamask?
    (-> db
        (assoc-in [:profile :address]
                  (u/getChecksumAddress (-> js/window .-web3 .-eth .-defaultAccount))
                  #_(get-in db [:drizzleStore :accounts :0]))
        (assoc-in [:profile :network] v) )
    ))

(re-frame/reg-event-db
  ::getLocalNetworkId
  (fn [db [_ _]]
    #_(log (u/getDrizzleWeb3js))
    (let [v (.getId (.-net (.-eth (u/getDrizzleWeb3js)))
                    (fn [e r]
                      (if (nil? e)
                        (do
                          (log (str ::getLocalNetworkId " " r "," r))
                          (re-frame/dispatch [::setLocalInfo r]))
                        (log (str ::getLocalNetworkId "ERROR: " e)))
                      ))]
      db)
    ))

;------ getting server's balance needs below three definitions
(re-frame/reg-event-db
  ::setServerBalance
  (fn [db [_ res]]
    (log (str ::setServerBalance " " res))
    (assoc-in db [:serverProfile :balance] #_(.fromWei (.-utils web3js) res "ether")
              res)))

(re-frame/reg-event-db
  ::setTokenBalance
  (fn [db [_ res]]
    (log (str ::setTokenBalance " " res))
    (assoc-in db [:tokenProfile :balance] #_(.fromWei (.-utils web3js) res "ether")
              res)))
(defn- setBalance [dispatchTo err res]
  (if (nil? err)
    (re-frame/dispatch [dispatchTo res])
    (log (str "web3js access was failed. " err))
    ))

(defn- getBalance [addr func]
  (.getBalance (.-eth (u/getDrizzleWeb3js)) addr func)
  )

(re-frame/reg-event-db
  ::getServerBalance
  (fn [db [_]]
    ;; just dispatching because the reply of getBalance is premise (async)
    ;; set func(serverBalance) as callback
    (let [serverAddr (get-in db [:serverProfile :address])]
      (if (not (nil? serverAddr))
        (getBalance serverAddr
                (partial setBalance ::setServerBalance))
        (prn "tokenAddr is nil")
        ))
    db)
  )

(re-frame/reg-event-db
  ::getTokenBalance
  (fn [db [_]]
    (let [tokenAddr (get-in db [:tokenProfile :address])]
      (if (not (nil? tokenAddr))
        (getBalance tokenAddr
                    (partial setBalance ::setTokenBalance))
        (prn "tokenAddr is nil")
        ))
    db)
  )
;------
(re-frame/reg-event-db
  ::checkMetamask
  (fn [db [_ _]]
    (let [m (u/checkMetamask)]
      (log (str ::checkMetamask " " (u/checkMetamask)))

      (if (not= m 2)
        (-> db
            (assoc-in [:profile :address] nil)
            (assoc-in [:profile :metamask] m))
        (assoc-in db [:profile :metamask] m)) )
    ))

(re-frame/reg-event-db
  ::setDialog
  (fn [db [_ target status]]
    (assoc-in db [:dialog target] status)
    ))

(re-frame/reg-event-db
  ::copyTokenPriceToTmp
  (fn [db [_ id]]
    (let [p (u/getTokenPriceByTokenId db (js/parseInt id))]
      (assoc-in db [:tmp (keyword (str "price_" id))] (eu/from-wei p)))
    ))
