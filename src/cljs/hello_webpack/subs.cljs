(ns hello-webpack.subs
  (:require [re-frame.core :as re-frame]
            [hello-webpack.events :as e]
            [hello-webpack.store :as s]
            [hello-webpack.utils :as u]
            [hello-webpack.reactable :as rtable])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(re-frame/reg-sub
  ::name
  (fn [db]
    (:name db)))

(def getAddress (fn [db] (get (get-in db [:drizzleStore :accounts]) :0)))
(def getBalance (fn [db] (let [a (getAddress db)]
                           (get-in db [:drizzleStore :accountBalances (keyword a)]))))

(re-frame/reg-sub
  ::account
  (fn [db]
    (getAddress db)))

(re-frame/reg-sub
  ::balance
  (fn [db]
    (getBalance db)))

(re-frame/reg-sub
  ::total
  (fn [db]
    #_(-> db :drizzleContracts :MyToken :totalSupply :0x0 :value)
    (u/getTokenTotal db)))

(re-frame/reg-sub
  ::tokenId
  (fn [db]
    (:tokenId db)))

(re-frame/reg-sub
  ::tokenMsg
  (fn [db]
    (:tokenMsg db)))

(re-frame/reg-sub
  ::allMsgs
  (fn [db]
    #_(str (get-in db [:drizzleContracts :MyToken]))
    (with-out-str (cljs.pprint/pprint (s/getStoreStateMyTokenClj)
                                      #_(get-in db [:drizzleContracts :MyToken])))))

(re-frame/reg-sub
  ::tmp
  (fn [db x]
    (get-in db [:tmp :x])))

(re-frame/reg-sub
  ::page
  (fn [db]
    (:page db)))

(re-frame/reg-sub
  ::drawerOpen
  (fn [db]
    (:drawer db)))

(re-frame/reg-sub
  ::serverProfile
  (fn [db [_ flag]]
    (case flag
      :address (get-in db [:serverProfile :address])
      :balance (get-in db [:serverProfile :balance])
      :network (get-in db [:serverProfile :network])
      "Unknown flag was found @subs")
    ))

(re-frame/reg-sub
  ::tokenProfile
  (fn [db [_ flag]]
    (case flag
      :address (get-in db [:tokenProfile :address])
      :balance (get-in db [:tokenProfile :balance])
      "Unknown flag was found @subs")
    ))

(re-frame/reg-sub
  ::tokenSoldTableData
  (fn [db]
    (rtable/convTableData :sold (rtable/mkMyPageTableData db))
    #_(:tokenSoldTableData db)
    ))

(re-frame/reg-sub
  ::tokenSellerTableData
  (fn [db]
    (rtable/convTableData :seller nil)
    #_(:tokenSellerTableData db)
    ))

(re-frame/reg-sub
  ::tokenBuyerTableData
  (fn [db]
    (rtable/convTableData :buyer nil)
    #_(:tokenBuyerTableData db)
    ))

(re-frame/reg-sub
  ::tokenPastTableData
  (fn [db]
    ;; we have no need to get from db
    ;; because the data for table also made via re-frame's event and
    ;; it means app-db cannot be updated directly with page change.
    ;;
    ;; page(event) <- page_data(event)
    ;;
   (let [x (rtable/convTableData :past nil)]
     #_(re-frame/dispatch [::e/setMyPageTables :past x])
     #_(log (str "past: " x))
     x)

    #_(:tokenPastTableData db)
    ))

(re-frame/reg-sub
  ::tokenDebugTableData
  (fn [db]
    (:tokenDebugTableData db)))

(re-frame/reg-sub
  ::searchTableData
  (fn [db]
    (:searchTableData db)))

(re-frame/reg-sub
  ::profile
  (fn [db [_ x]]
    (get-in db [:profile x])))

(re-frame/reg-sub
  ::profilesAll
  (fn [db [_ _]]
    (get-in db [:profiles])))

(re-frame/reg-sub
  ::profiles
  (fn [db [_ x]]
    (get-in db [:profiles (keyword x)])))

(re-frame/reg-sub
  ::search
  (fn [db [_ x]]
    (get-in db [:search x])))

(re-frame/reg-sub
  ::contents
  (fn [db [_ x]]
    (get-in db [:contents x])))

#_(re-frame/reg-sub
  ::contentsTable
  (fn [db [_ _]]
    (get-in db [:contents :lists])))

(re-frame/reg-sub
  ::linkedIn
  (fn [db [_ x]]
    (get-in db [:linkedIn x])))

(re-frame/reg-sub
  ::profileLinkedIn
  (fn [db [_ x]]
    (get-in db [:profile :linkedIn x])))

(re-frame/reg-sub
  ::dialog
  (fn [db [_ x]]
    (get-in db [:dialog x])))

(re-frame/reg-sub
  ::selectedCourse
  (fn [db [_ _]]
    (get-in db [:selectedCourse])))
