(ns hello-webpack.ajax
  (:require [ajax.core :as ajax]
            [re-frame.core :as re-frame]
            [day8.re-frame.http-fx]
            [hello-webpack.utils :as u]
            [cljs-time.core :as tc]
            [cljs-time.format :as tf]
            [hello-webpack.reactable :as rtable])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(re-frame/reg-event-db
  ::loadProfileSuccess
  (fn [db [_ result] ]
    (let [res (:result result)]
      (log (str ::loadProfileSuccess "\n" res))
      (if (= (:email res) (get-in db [:profile :email]))
        (-> db
            ;(assoc-in [:profile :headline] (get-in res [:linkedIn :headline]))
            ;(assoc-in [:profile :linkedInUrl] (get-in res [:linkedIn :siteStandardProfileRequest :url]))
            (assoc-in [:profile] res))  ;TODO
        (assoc-in db [:profiles (keyword (:address res))] res)))
  ))

(re-frame/reg-event-fx
  ::loadProfile
  (fn [{:keys [db]} [_ p]]
    (let [pp {:address (if (not (nil? p))
                         p
                         (get-in db [:profile :email]))}]
      (if (contains? (:profiles db) (keyword (:address pp)))
        (do (log (str ::loadProfile " Found on :profiles " (:address pp))) {:db db})                                            ;; causes the twirly-waiting-dialog to show??
        {:db         db                                     ;; causes the twirly-waiting-dialog to show??
         :http-xhrio {:method          :get
                      :uri             "/profile"
                      :timeout         3000                 ;; optional see API docs
                      :params          pp
                      :format          (ajax/json-request-format)
                      :response-format (ajax/json-response-format {:keywords? true})
                      :on-success      [::loadProfileSuccess]
                      :on-failure      [::u/apiAccessFailed]}}))
    ))

(re-frame/reg-event-db
  ::setEventInfo
  (fn [db [_ info]]
    (let [res (:result info)
          format (tf/formatters :date-time)]
      (log (str ::setEventInfo "\n" (select-keys res [:tokenId :summary :creator :start :end])))
      ;(log (tf/parse  (get-in res [:start :dateTime])))
      (-> db
          (assoc-in [:events (keyword (str (:tokenId (:result info))))]
                    (-> res
                        (assoc-in [:start :dateTime] (tf/parse format (get-in res [:start :dateTime])))
                        (assoc-in [:end :dateTime] (tf/parse format (get-in res [:end :dateTime])))))
          ))
    ))

(re-frame/reg-event-fx
  ::getEventInfo
  (fn [{:keys [db]} [_ tokenId]]
    (log (str ::getEventInfo " http"))
    {:db         (assoc db :show-twirly true)   ;; causes the twirly-waiting-dialog to show??
     :http-xhrio {:method          :get
                  :uri             "/event"
                  :params          {:id tokenId}
                  :timeout         3000   ;; optional see API docs
                  :response-format (ajax/json-response-format {:keywords? true})  ;; IMPORTANT!: You must provide this.
                  :on-success      [::setEventInfo]
                  :on-failure      [::u/apiAccessFailed]}}))

(re-frame/reg-event-db
  ::getServerInfoSuccess
  (fn [db [_ info]]
    (log (str ::getServerInfoSuccess " " info))
    (-> db
        (assoc-in [:tokenProfile :address] (u/getChecksumAddress
                                             (:tokenAddr info)) )
        (assoc-in [:serverProfile :address] (u/getChecksumAddress
                                              (:address info)) )
        (assoc-in [:serverProfile :network] (:network info)))
    ))

(re-frame/reg-event-fx
  ::getServerInfo
  (fn [{:keys [db]} _]
    (log (str "getServerInfo: http"))
    {:db   (assoc db :show-twirly true)   ;; causes the twirly-waiting-dialog to show??
     :http-xhrio {:method          :get
                  :uri             "/serverInfo"
                  :timeout         3000   ;; optional see API docs
                  :response-format (ajax/json-response-format {:keywords? true})  ;; IMPORTANT!: You must provide this.
                  :on-success      [::getServerInfoSuccess]
                  :on-failure      [::u/apiAccessFailed]}}))

(re-frame/reg-event-db
  ::saveProfileSuccess
  (fn [db [_ msg]]
    (log (str ::saveProfileSuccess " " msg))
    db))

(re-frame/reg-event-fx
  ::saveProfile
  (fn [{:keys [db]} [_ _]]
    (if (and (> (count (keys (:profile db))) 2))
      {:db         db
       :http-xhrio {:method          :post
                    :uri             "/profile"
                    :timeout         3000
                    :params          (dissoc (:profile db) :approved :metamask :network)
                    :format          (ajax/json-request-format)
                    :response-format (ajax/json-response-format {:keywords? true}) ;; IMPORTANT!: You must provide this.
                    :on-success      [::saveProfileSuccess]
                    :on-failure      [::u/apiAccessFailed]}}
      (do
        (log (str ::saveProfile " skipped.\n" (:profile db)))
        {:db db})
      )))

(re-frame/reg-event-db
  ::doneSetQuillContent
  (fn [db [_ content]]
    (re-frame/dispatch [::getContentsInfo])
    (log (str "quill: " content))
    (assoc-in db [:contents :new] "")
    ))

(re-frame/reg-event-fx
  ::setQuillContent
  (fn [{:keys [db]} [_ _]]
    (log (str ::setQuillContent " http" (get-in db [:contents :new])))
    {:db         (assoc db :show-twirly true)   ;; causes the twirly-waiting-dialog to show??
     :http-xhrio {:method          :post
                  :uri             "/content"
                  :params          {:address (u/addr db)
                                    :id      (get-in db [:contents :number])
                                    :content {:html    (get-in db [:contents :new])
                                              :created (tc/now)}}
                  :timeout         3000                     ;; optional see API docs
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true}) ;; IMPORTANT!: You must provide this.
                  :on-success      [::doneSetQuillContent]
                  :on-failure      [::u/apiAccessFailed]}}))

(defn mkContentsTable [result]
  (into [] (map (fn [x] {:id        (name x)
                         :content   [:div {"dangerouslySetInnerHTML" #js{:__html
                                                                         (get-in (get result x) [:html])}}]
                         :created   (get-in (get result x) [:created :date])
                         :operation [:button {:on-click #(re-frame/dispatch [::ajax/deleteDbItem "content" (name x)])}
                                     "del"]})
                (keys result)))
  )

(re-frame/reg-event-db
  ::getContentsInfoSuccess
  (fn [db [_ contents]]
    (log (str contents "," (keys (:result contents))))
    (let [result (:result contents)
          data (mkContentsTable result)]
      #_(log (str ::getContentsInfoSuccess "\n" data))
      (-> db
          (assoc-in [:contents :number] (inc (count (keys result)))) ;TODO cannot delete with this logic..
          (assoc-in [:contents :lists] data)))
    ))

(re-frame/reg-event-fx
  ::getContentsInfo
  (fn [{:keys [db]} [_ addr]]
    (log (str ::getContentsInfo " http"))
    {:db         (assoc db :show-twirly true)   ;; causes the twirly-waiting-dialog to show??
     :http-xhrio {:method          :get
                  :uri             "/contents"
                  :params          {:address (u/addr db)}
                  :timeout         3000                     ;; optional see API docs
                  :response-format (ajax/json-response-format {:keywords? true}) ;; IMPORTANT!: You must provide this.
                  :on-success      [::getContentsInfoSuccess]
                  :on-failure      [::u/apiAccessFailed]}}))

(re-frame/reg-event-db
  ::doneDeleteDbItem
  (fn [db [_ content]]
    (let [id (:id content)
          category (:category content)]
      (log (str "db item deleted: " category "," id))
      (if (= category "content")
        (re-frame/dispatch [::getContentsInfo])))
    db
    ))

(re-frame/reg-event-fx
  ::deleteDbItem
  (fn [{:keys [db]} [_ category id]]
    (log (str ::deleteDbItem " http " category ", " id))
    {:db         db
     :http-xhrio {:method          :delete
                  :uri             (str "/" category)
                  :params          {:address (u/addr db)
                                    :id      id}
                  :timeout         3000
                  :format          (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true}) ;; IMPORTANT!: You must provide this.
                  :on-success      [::doneDeleteDbItem]
                  :on-failure      [::u/apiAccessFailed]}})
  )
;
;(defn setEventInfo2 [ok info]
;  (let [res (:result info)
;        format (tf/formatters :date-time)]
;    (log (str "setEventInfo2\n" (select-keys res [:tokenId :summary :creator :start :end])))
;    {:start {:dateTime (tf/parse format (get-in res [:start :dateTime]))}
;     :end {:dateTime (tf/parse format (get-in res [:end :dateTime]))}}
;    )
;  )
;
;(defn handler2 [[ok response]]
;  (if ok
;    (.log js/console (str response))
;    (.error js/console (str response))))
;
;(defn getViaHttp [category id]
;  (ajax/ajax-request
;    {:method          :get
;     :uri             (case category
;                        :event "/event"
;                        :profile "/profile")
;     :params          {:id id}
;     :timeout         3000   ;; optional see API docs
;     :response-format (ajax/json-response-format {:keywords? true})  ;; IMPORTANT!: You must provide this.
;     :handler handler2}
;  )
;  )