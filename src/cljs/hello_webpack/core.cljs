(ns hello-webpack.core
  (:require [reagent.core :as r]
            [re-frame.core :as re-frame]
            [hello-webpack.views :as views]
            [hello-webpack.config :as config]
            [hello-webpack.store :as s]
            [hello-webpack.events :as e]
            ))

;;
;; https://maksimivanov.com/posts/ethereum-react-dapp-tutorial-part-2/
;; https://github.com/satansdeer/ethereum-token-tutorial
;; https://qiita.com/shora_kujira16/items/6efd834e6a3a3eb8a98c
;;

(enable-console-print!)

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (r/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (re-frame/dispatch-sync [::e/init])
  #_(dev-setup)
  (mount-root) )

;; will be called after having a successful login to Google
(defn ^:export setProfile [x]
  (re-frame/dispatch [::e/setGoogleProfile x]) )

;(defn ^:export buyClick [eventId fromAddr tokenId]
;  (re-frame/dispatch [::ee/mytokenBuyToken eventId fromAddr tokenId]) )
;
;(defn ^:export setPrice [tokenId price]
;  (re-frame/dispatch [::ee/mytokenSetPrice price tokenId])
;  false)