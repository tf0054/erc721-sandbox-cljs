(ns hello-webpack.store)

(def Drizzle (.getDrizzle js/Drizzle))

(defn- mkOptionsForDrizzle [x]
  (clj->js {:contracts [(js->clj x)]
            }))

(defn- mkOptionsObj [x]
  (clj->js {:web3      {:block    false
                        :fallback {:type "ws"
                                   :url  "ws://127.0.0.1:8545"}}
            :contracts [(js->clj x)]
            :event     {}
            }))

(def drizzleStore (.generateStore Drizzle (mkOptionsForDrizzle js/ContractJSON)))

(def drizzle (new (.-Drizzle Drizzle) (mkOptionsObj js/ContractJSON) drizzleStore))

#_(defn listenLog []
        (.log js/console "store's listener was called!")    )

(defn setStoreListener [x]
      (.subscribe drizzleStore x)
      )

(defn getStoreStateClj []
  (js->clj (-> drizzle .-store .getState) :keywordize-keys true))

(defn getStoreStateMyTokenClj []
  (-> (getStoreStateClj)
      :contracts
      :MyToken)
  )