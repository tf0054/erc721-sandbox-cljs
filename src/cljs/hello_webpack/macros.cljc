(ns hello-webpack.macros)

(defmacro log
    [& msgs]
      `(.log js/console ~@msgs))
