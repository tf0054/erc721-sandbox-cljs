(ns hello-webpack.drizzle
  (:require [cljsjs.material-ui]
            [material-ui-icons]
            #_[cljsjs.material-ui-svg-icons]
            [re-frame.core :as re-frame]
            [hello-webpack.utils :as u]
            [hello-webpack.ajax :as ajax]
            [hello-webpack.ethereum.events :as ee]
            [hello-webpack.search.events :as se]
            [hello-webpack.store :as s]
            )
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

(defn setDrizzleStoreListener []
  (log (str "setting a listener on the redux store"))
  (s/setStoreListener (fn []
                        (re-frame/dispatch [::sync-db]))) )

(defn- checkDiff [db diffContractsSync newStore]
  (let [processes {:initialized #(re-frame/dispatch [::drizzle-initialized])
                   :synced #(if (not (:firstSync db))
                              (re-frame/dispatch [::drizzle-started]) ; called just once (first-sync)
                              (re-frame/dispatch [::drizzle-synced (dissoc newStore :contracts)]))
                   }]
  ;; 1st level
  #_(log (str diffContractsSync))
  (if (and (contains? diffContractsSync :initialized)
           (= true (:initialized diffContractsSync)))
    ((:initialized processes)))
  ;; 1st level
  (if (and (contains? diffContractsSync :synced)
           (= true (:synced diffContractsSync)))
    ((:synced processes))) ; everytime "synced" after first-sync
  ;; lower level check needed.
  (if (contains? diffContractsSync :totalSupply)
    (re-frame/dispatch [::doneGetTotalSupply diffContractsSync]))
  ;; trigger of updating table data which can be found on MyPage and search
  (if (or #_(contains? diffContractsSync :tokenURI)
          (contains? diffContractsSync :ownerOf)
          (contains? diffContractsSync :getHost)
          (contains? diffContractsSync :getPrice))
    (case (:page db)
      ;; TODO not needed? seems updated automatically
      "second" (re-frame/dispatch [::u/pageChange "second"])
      ;; TODO firstly update algolia and after that search table should be updated.
      "search" (re-frame/dispatch [::se/getSearchCardsFacets (get-in db [:search :word])])
      (log (str "No updates will be made with redux changes. (" (:page db) ")" ))
      ))
  ;; trigger of updating table data
  (if (contains? diffContractsSync :transactions)
    (re-frame/dispatch [::transactionsChange diffContractsSync]))
  ;; lower level check needed.
  (if (contains? diffContractsSync :isApprovedForAll)
    (if (not (empty? (:isApprovedForAll diffContractsSync)))
      (let [ids (keys (:isApprovedForAll diffContractsSync))
            value (:value (get (:isApprovedForAll diffContractsSync) (first ids)))]
        (if value
          (do (log (str "Tokimo has been approved: " value ", so jumping to the token list page."))
              (re-frame/dispatch [::isApprovedForAllsetSuccess])
              )
          (do (log (str "Tokimo hasnt been approved: " value ", so going to approval page."))
              (re-frame/dispatch [::u/pageChange "approval"]))))
      (log "Seems approval isnt provided.")
      ))
  ))

(re-frame/reg-event-db
  ::drizzle-initialized
  u/interceptors
  (fn [db [_ _]]
    (log (str ::drizzle-initialized))
    db))

(re-frame/reg-event-db
  ::drizzle-started
  u/interceptors
  (fn [db [_ _]]
    (log (str ::drizzle-started))
    (assoc-in db [:firstSync] true)
    ))

(re-frame/reg-event-db
  ::drizzle-synced
  (fn [db [_ newStore]]
    ;; This can be called only with some modification (not just calling methods on contract)
    (log (str ::drizzle-synced " " (ee/txAndStatus newStore)))
    ;;
    (re-frame/dispatch [::ee/getServerBalance])
    (re-frame/dispatch [::ee/getTokenBalance])
    (re-frame/dispatch [::ee/mytokenGetTotal])
    db))

(re-frame/reg-event-db
  ::sync-db
  u/interceptors
  (fn [db [_ x]]
    (let [newStore (s/getStoreStateClj)]
      (if (-> newStore :drizzleStatus :initialized)
        (let [diffContractsSync (u/diffMap (-> db
                                               :drizzleContracts
                                               :MyToken)
                                           (-> newStore
                                               :contracts
                                               :MyToken))]
          #_(log (str ::sync-db diffContractsSync))
          (if (not (nil? diffContractsSync))
            (checkDiff db diffContractsSync newStore) )
          (-> db
              (assoc-in [:drizzleStore] (dissoc newStore :contracts))
              (assoc-in [:drizzleContracts] (:contracts newStore))))
        (do
          (log (str "Waiting drizzle to be initialized." #_newStore))
          db))
      ))
  )

(re-frame/reg-event-db
  ::transactionsChange
  u/interceptors
  (fn [db [_ c]]
    (log (str ::transactionsChange c (-> db :drizzleStore :transactions)))
    db))

(re-frame/reg-event-db
  ::isApprovedForAllsetSuccess
  (fn [db [_ _]]
    (log (str ::isApprovedForAllsetSuccess "\napproval check was successfully finished."))
    (if-let [buyParams (get-in db [:stashedPurchase])]
      (do (log "Resuming previous purchase: " buyParams)
          (re-frame/dispatch
            (into [] (concat [::ee/mytokenBuyToken] buyParams)) ))
      (log "There is no ongoing transaction"))
    (-> db
        (assoc-in [:stashedPurchase] nil)
        (assoc-in [:profile :approved] true))
    ))

(re-frame/reg-event-db
  ::doneUpdateOwnerByAll
  (fn [db [_ _]]
    (log (str ::doneUpdateOwnerByAll))
    db))

(re-frame/reg-event-db
  ::updateOwnerByAll
  (fn [db [_ _]]
    (let [total (u/getTokenTotal db)]
      (log (str ::updateOwnerByAll " " (u/getTokenTotal db))) ;NaN can be happened.
      (if (not (js/isNaN total))
        (do
          (doall (map (fn [id]
                        (ee/getTokenOwnerOfByIndex id))
                      (range 0 (u/getTokenTotal db))))
          (re-frame/dispatch [::doneUpdateOwnerByAll]))
        (u/wait #(re-frame/dispatch [::updateOwnerByAll]) 500) ) )
     db)
  )

(re-frame/reg-event-db
  ::updateAll
  (fn [db [_ init?]]
    (let [maxId (u/getTokenTotal db)
          tokenHashes (u/tokenHashFromSortedOwnerOf db)
          syncStart (if init?
                      0
                      (if (empty? tokenHashes)
                        0
                        (count tokenHashes)))]

      (log (str ::updateAll " " syncStart "-" maxId))
      (doall (map (fn [id]
                    ;; cannot call in event-hander as synced
                    ;;(re-frame/dispatch-sync [::mytokenGetTotal])
                    (re-frame/dispatch [::ee/mytokenGetUrlByIndex id])
                    (re-frame/dispatch [::ee/mytokenOwnerOfByIndex id])
                    (re-frame/dispatch [::ee/mytokenGetPrice id])
                    (re-frame/dispatch [::ee/mytokenGetHost id])
                    (re-frame/dispatch [::ajax/getEventInfo id]))
                  (range syncStart maxId)))
      )
    db)
  )

(re-frame/reg-event-db
  ::doneGetTotalSupply
  (fn [db [_ diffContractsSync]]
    (if (not (empty? (:totalSupply diffContractsSync)))
      (do (let [totalSupply (js/parseInt
                              (-> diffContractsSync :totalSupply :0x0 :value))]
            (if (> totalSupply 0)
              (do
                (re-frame/dispatch [::updateAll false])
                (log (str "totalSupply " totalSupply
                          " was gotten by drizzle but it might need time to sync to app-db.\n"
                          "Waiting logic is in " ::updateOwnerByAll))
                ))))
      )
    db))

;---
#_(re-frame/reg-event-db
  ::setTokenAddr
  (fn [db [_ msg]]
    (log (str ::setTokenAddr " " msg))
    (assoc-in db [:tokenAddr] msg)
    ))

#_(re-frame/reg-event-db
  ::getTokenAddr
  (fn [db [_ _]]
    (let [DrizzleContractObj (aget (-> s/drizzle .-contractList) 0)
          msg (.-address DrizzleContractObj)]
      (if (not (nil? msg))
        (re-frame/dispatch [::setTokenAddr msg]))
      db
      )))
