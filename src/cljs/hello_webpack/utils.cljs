(ns hello-webpack.utils
  (:require [hello-webpack.store :as s]
            [re-frame.core :as re-frame])
  (:require-macros
    [hello-webpack.macros :refer [log]])
  )

;----
(defn compNested [x]
  (get-in (second x) [:args :0]) )

(defn addr [db] (get-in db [:profile :address]))
(defn sortedOwnerOfs [db] (let [x (sort-by compNested (:ownerOf (s/getStoreStateMyTokenClj))
                                          #_(get-in db [:drizzleContracts :MyToken :ownerOf]))]
                           #_(log (str "Sorted? \n" (:ownerOf (s/getStoreStateMyTokenClj)) "\n" x))
                           x))
(defn tokenHashFromSortedOwnerOf [db] (keys (sortedOwnerOfs db)) )

(defn getPrices [db] (:getPrice (s/getStoreStateMyTokenClj))
  #_(get-in db [:drizzleContracts :MyToken :getPrice]))
;;
(defn tokenIdByTokenHash [db keyHash] (get-in (:tokenURI (s/getStoreStateMyTokenClj)) [keyHash :args :0]))
(defn tokenURIByTokenHash [db keyHash] (get-in (:tokenURI (s/getStoreStateMyTokenClj)) [keyHash :value]))
(defn ownerOfByTokenHash [db keyHash] (get-in (:ownerOf (s/getStoreStateMyTokenClj)) [keyHash :value])
  #_(get-in db [:drizzleContracts :MyToken :ownerOf keyHash :value]))
(defn getPriceByTokenHash [db keyHash] (get-in (:getPrice (s/getStoreStateMyTokenClj)) [keyHash :value])
  #_(get-in db [:drizzleContracts :MyToken :getPrice keyHash :value]))
(defn getHostByTokenHash [db keyHash] (get-in (:getHost (s/getStoreStateMyTokenClj)) [keyHash :value])
  #_(get-in db [:drizzleContracts :MyToken :getHost keyHash :value]))
;;
(defn getTokenHostByTokenId [db tokenId] (getHostByTokenHash db (nth (tokenHashFromSortedOwnerOf db) tokenId)))
(defn getTokenOwnerByTokenId [db tokenId] (ownerOfByTokenHash db (nth (tokenHashFromSortedOwnerOf db) tokenId)))
(defn getTokenPriceByTokenId [db tokenId] (getPriceByTokenHash db (nth (tokenHashFromSortedOwnerOf db) tokenId)))

(defn getTokenAddr [db] (get-in db [:tokenProfile :address]))

(defn getTokenTotal [db]
                     (js/parseInt (get-in (s/getStoreStateMyTokenClj) [:totalSupply :0x0 :value])
                       #_(get-in db [:drizzleContracts :MyToken :totalSupply :0x0 :value])))

(defn getProfiles [db addr]
  (get-in db [:profiles (keyword addr)])
  )

;----
(defn diffMap [x y]
  (second (into [] (clojure.data/diff x y))) )

(def interceptors [#_(when ^boolean js/goog.DEBUG debug)
                   re-frame/trim-v])

(re-frame/reg-event-db
  ::pageChange
  (fn [db [_ p]]
    (.page js/drift p)
    (assoc-in db [:page] p)
    ))

(re-frame/reg-event-db
  ::apiAccessFailed
  (fn [db [_ id]]
    (prn ::apiAccessFailed "http access failed:" id)
    db
    ))

(re-frame/reg-event-db
  ::setTmp
  (fn [db [_ x v]]
    (assoc-in db [:tmp x] v)
    ))

(defn checkMetamask []
  (try (let [currentProvider (-> js/window .-web3 .-currentProvider .-constructor .-name)]
         (if (= "MetamaskInpageProvider" currentProvider)
           (if (nil? (-> js/window .-web3 .-eth .-defaultAccount))
             1 ;installed(NotLoggedIn)
             2 ;loggedIn
             )
           0)) ;NotInstalled
       (catch js/Error e 0)
  ))

(defn getDrizzleWeb3js []
  (-> s/drizzle .-web3))

(defn getMyTokenMethods []
  (-> s/drizzle .-contracts .-MyToken .-methods))

(defn getChecksumAddress [addr]
  (.toChecksumAddress (.-utils (getDrizzleWeb3js)) addr)
  )

(defn e-val [e]
  (aget e "target" "value"))

(defn shortenStr [x]
  (str (subs x 0 17) "..")
  )

(defn cutDetailedTime [x]
  (clojure.string/replace x #":00\.000\+09:00" " (JST)")
  )

(defn wait [func millisec]
  (js/setTimeout func millisec)
  )

(defn setTitle [title]
  (set! js/document.title title)
  )

(re-frame/reg-sub
  ::getTokenPriceCP
  (fn [db [_ id]]
    (get-in db [:tmp (keyword (str "price_" id))])))