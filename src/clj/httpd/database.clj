(ns httpd.database
  (:require [clojure.data :as d]
            [codax.core :as cc]
            [httpd.components.codax :as co]
            [clojure.tools.logging :as log]))

(defn getProfiles []
  (cc/get-at! @co/dbf [:profile])
  )

(defn setProfile [x]
  (let [keyAddr (keyword (:address x))
        keyEmail (keyword (:email x))]
    #_(log/info "Number of profiles: " (count (keys (getProfiles))))
    ;;
    (if (nil? keyAddr)
      (do (log/warn (str "Cannot store ethKey->profile. Maybe Metamask login needed?"))
          )
      (cc/assoc-at! @co/dbf [:profile keyAddr] x))
    (do
      (if (not (nil? (cc/get-at! @co/dbf [:profile keyAddr])))
        (log/warn (str keyAddr " has been stored already.")))

      (cc/assoc-at! @co/dbf [:profile keyEmail] x)
      {:status "OK" :result (str keyAddr ", " keyEmail)})
    )
  )

(defn delProfile [x]
  (cc/dissoc-at! @co/dbf [:profile (keyword nil)])
  (cc/dissoc-at! @co/dbf [:event (keyword nil)])
  (let [keyId (keyword (:id x))]
    (if-let [x (cc/get-at! @co/dbf [:profile keyId])]
      (do (log/info "An event" keyId "was deleted(renamed).")
          (cc/dissoc-at! @co/dbf [:profile keyId])
          (cc/assoc-at! @co/dbf [:profile (keyword (str "_" (:id x)))] x)
          {:status "OK" :result (str keyId " was Deleted.")} )
      {:status "NG" :result (str "An event (" keyId ") wasnt found.")}
      )
    ))

(defn getProfile [addr]
  #_(log/info "getProfile:" addr "," (getProfiles))
  (if-let [p (cc/get-at! @co/dbf [:profile (keyword addr)])]
    (do (log/info "Profile was found among" (count (keys (getProfiles))) ":" p)
        p))
  )
;---
(defn getEvents []
  (cc/get-at! @co/dbf [:event])
  )

(defn getEvent [keyId]
  (cc/get-at! @co/dbf [:event keyId])
  )

(defn setEvent [x]
  (let [keyId (keyword (:id x))
        keysOfEvents (keys (getEvents))]
    (log/info "Number of events:"
              (count (filter #(clojure.string/starts-with? % "_") keysOfEvents)) "/"
              (count keysOfEvents))
    ;;
    (if-let [newX (getEvent keyId)]
      (do
        (log/warn (str keyId " has been stored already. "
                       (second (into [] (d/diff x newX)))))
        (if (:merge x)
          (do
            (if-let [tokenId (:tokenId x)]
              (cc/assoc-at! @co/dbf [:event (keyword (str tokenId))] (dissoc x :merge)))
            (cc/assoc-at! @co/dbf [:event keyId] (dissoc x :merge))
            {:status "OK" :result (str "overwrited and tokenId index was prepared. " (:tokenId x))})
          {:status "NG" :result "nothing done bc it has no :merge."}))
      (do
        (if-let [tokenId (:tokenId x)]
          (cc/assoc-at! @co/dbf [:event (keyword (str tokenId))] x))
        (cc/assoc-at! @co/dbf [:event keyId] x)
        {:status "OK" :result (str "Event " keyId " was stored.")})))
  )

(defn delEvent [x]
  (let [keyId (keyword (:id x))]
    (if-let [x (cc/get-at! @co/dbf [:event keyId])]
      (let [keyTokenId (keyword (:tokenId x))]
        (log/info "An event" keyId "was deleted(renamed).")
        (cc/dissoc-at! @co/dbf [:event keyId])
        (cc/assoc-at! @co/dbf [:event (keyword (str "_" (:id x)))] x)
        (cc/dissoc-at! @co/dbf [:event keyTokenId])
        (cc/assoc-at! @co/dbf [:event (keyword (str "_" (:tokenId x)))] x)
        {:status "OK" :result (str "Event " (:id x) "(" (:tokenId x) ") was deleted.")} )
      {:status "NG" :result (str "An event (" keyId ") wasnt found.")} )
    ))

;---
(defn getContentsAll []
  (cc/get-at! @co/dbf [:content])
  )
(defn getContents [addr]
  (cc/get-at! @co/dbf [:content (keyword addr)])
  )

(defn getContent [addr id]
  (cc/get-at! @co/dbf [:content (keyword addr) (keyword (str id))])
  )

(defn delContent [x]
  (cc/dissoc-at! @co/dbf [:content (keyword (:address x)) (keyword (str (:id x)))])
  )

(defn setContent [x]
  (let [addrId (keyword (:address x))
        keyId (keyword (str (:id x)))
        clength (count (:content x))]
    (if (> clength 0)
      (do
        #_(cc/dissoc-at! @co/dbf [:content addrId (keyword nil)])
        (log/info "Created a content with [" addrId " " keyId "] " (count (:content x)))
        (cc/assoc-at! @co/dbf [:content addrId keyId] (:content x))
        {:status "OK" :result "created."}
        )
      (do (log/warn "Content length seems not right.")
          {:status "NG" :result "cannot created."}
          )
      )
    ))