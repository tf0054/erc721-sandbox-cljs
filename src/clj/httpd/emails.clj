(ns httpd.emails
  (:require [cheshire.core :as c]
            [clojure.tools.logging :as log]
            [httpd.utils :as u]
            [httpd.comm :as comm]))

(defn mkMailBasic [info ^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (let [data (c/generate-string {:to      (:to info)
                                 :subject (str "[LALA] Token(Number " (u/getTokenIdFromReceipt r)
                                               ") was assigned!")
                                 :body    (str (.getStatus r) "\n\n"
                                               (.toString r) "\n\n"
                                               (:htmlLink info)
                                               )})]
    (log/info "mkMailBasic: " (:to info) (:htmlLink info))
    data)
  )

(defn mkMailCancel [info]
  (let [data (c/generate-string {:to      (get-in info [:to])
                                 :subject (str "[LALA] Token(Number " (:tokenId info)
                                               ") was successfully cancelled by " (:who info))
                                 :body    (str "deleted event: " (:summary info) ", "
                                               (get-in info [:start :dateTime]) " - "
                                               (get-in info [:end :dateTime]) "\n\n"
                                               info
                                               )})]
    (log/info "mkMailCancel: " (:tokenId info))
    data)
  )

(defn mkMailCancelled [info]
  (let [data (c/generate-string {:to      (get-in info [:to])
                                 :subject (str "[LALA] Token(Number " (:tokenId info)
                                               ") was cancelled by " (:who info))
                                 :body    (str "deleted event: " (:summary info) ", "
                                               (get-in info [:start :dateTime]) " - "
                                               (get-in info [:end :dateTime]) "\n\n"
                                               info
                                               )})]
    (log/info "mkMailCancelled: " (:tokenId info))
    data)
  )

(defn mkMailBurn [info ^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (let [data (c/generate-string {:to      (get-in info [:creator :email])
                                 :subject (str "[LALA] Token(Number " (:tokenId info)
                                               ") was burned!")
                                 :body    (str "deleted event: " (:summary info) ", "
                                               (get-in info [:start :dateTime]) " - "
                                               (get-in info [:end :dateTime]) "\n\n"
                                               (.getStatus r) "\n\n"
                                               (.toString r)"\n\n" info
                                               )})]
    (log/info "mkMailBurn: " (:tokenId info))
    data)
  )

(defn mkMailMoveTo [event to ^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (let [data (c/generate-string {:to      to
                                 :subject (str "[Tokhimo] Token(Number " (:tokenId event) #_(u/getTokenIdFromReceipt r)
                                               ") was yours!")
                                 :body    (str (.getStatus r) "\n\n"
                                               event "\n\n"
                                               (.toString r))})]
    (log/info "mkMailMoveTo:" to)
    data)
  )

(defn mkMailMoveFrom [event to ^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (let [data (c/generate-string {:to      to
                                 :subject (str "[Tokhimo] Token(Number " (:tokenId event) #_(u/getTokenIdFromReceipt r)
                                               ") was sold!")
                                 :body    (str (.getStatus r) "\n\n"
                                               event "\n\n"
                                               (.toString r))})]
    (log/info "mkMailMoveFrom:" to)
    data)
  )

(defn mkEventAttendeeUpdate [info ^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (let [data (c/generate-string {:eventId (:id info) ; info = google.cal.Event
                                 :status  "accepted"
                                 :comment (str "[Tokhimo] Token(Number " (u/getTokenIdFromReceipt r)
                                               ") was assigned on block " (.getBlockHash r))})]
    (log/info "mkEventAtendeeUpdate:" (:id info))
    data)
  )

(defn mkEventForDatabase [info ^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (let [event (:result (comm/getEvent (:id info)))]
    (let [updateData (merge event
                            {:merge true}
                            {:tokenId (u/getTokenIdFromReceipt r)})]
      (log/info "mkEventForDatabase/Updated:" updateData)
      (c/generate-string updateData))
      )
    )