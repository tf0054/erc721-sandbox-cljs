(ns httpd.components.codax
  (require [codax.core :as c]
           [clojure.tools.logging :as log])
  )

(def dbf (atom nil)) ;

(defn opendbf []
  (reset! dbf (c/open-database! "database"))
  )

(defn testdbf []
  (c/assoc-at! @dbf [:assets :people] {0 {:name       "Alice"
                                          :occupation "Programmer"
                                          :age        42}
                                       1 {:name       "Bob"
                                          :occupation "Writer"
                                          :age        27}}) ; {0 {:age 42, :name "Alice", ...}, 1 {:age 27, :name "Bob", ...}}

  (c/get-at! @dbf [:assets :people 0])                      ; {:name "Alice" :occupation "Programmer" :age 42}

  (c/update-at! @dbf [:assets :people 1 :age] inc)          ; 28

  (c/merge-at! @dbf [:assets] {:tools {"hammer"   true
                                       "keyboard" true}})   ; {:people {...} :tools {"hammer" true, "keyboard" true}}

  (log/info (c/get-at! @dbf [:assets]))
;;  {:people {0 {:name "Alice"
;;               :occupation "Programmer"
;;               :age 42}
;;            1 {:name "Bob"
;;               :occupation "Writer"
;;               :age 28}}
;;   :tools {"hammer" true
;;           "keyboard" true}}
  )
(defn closedbf []
  (c/close-database! @dbf))