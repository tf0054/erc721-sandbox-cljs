(ns httpd.components.linkedin
  (:use compojure.core)
  (:require [clj-http.client :as client]
            [clojure.tools.logging :as log]
            [cheshire.core :as c]
            [httpd.database :as p]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; CONSTANTS
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def ENV "https://gke.digipepper.com:8081/linkedin")
(def API_KEY "8131p861gbdfqs")
(def SECRET_KEY "OuvTNcriC9kdn7DU")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; IN MEMORY
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def lIn-user (atom {:first_name "" :last_name ""}))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; LOGIC
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn getLinkedinAuthUrl [x]
  (str "https://www.linkedin.com/oauth/v2/authorization"
                              "?response_type=code"
                              "&state=" x
                              "&client_id=" API_KEY
                              "&redirect_uri=" ENV)
  )

; https://www.linkedin.com/oauth/v2/authorization?client_id=8131p861gbdfqs&response_type=code
;https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=123456789
; &redirect_uri=https%3A%2F%2Fwww.example.com%2Fauth%2Flinkedin&state=987654321&scope=r_basicprofile

(defn- welcome-page
  [user]
  (str "Hello there buddy, your name is " (user :first_name) " " (user :last_name)) )

(defn- request-access-token
  "Request the access token and return it."
  [authorization_token]
  (-> (client/post (str "https://www.linkedin.com/oauth/v2/accessToken?"
                        "grant_type=authorization_code"
                        "&code=" authorization_token
                        "&redirect_uri=" ENV
                        "&client_id=" API_KEY
                        "&client_secret=" SECRET_KEY))
      :body
      (c/parse-string)
      (get "access_token")))

(defn- profile-request
  "This is just a simple request to the LinkedIn API,
  where we are going to get our profile data."
  [access_token state]
  (log/info "LinkedIn accessToken: " access_token ", " state)
  (let [linkedInProfile
        (-> (client/get
              (str "https://api.linkedin.com/v1/people/~"
                   "?format=json")
              #_(str "https://api.linkedin.com/v2/me")
                        {:headers {:Authorization (str "Bearer " access_token)} }
              )
            :body
            (c/parse-string true))]
    ;(rename-keys {:a 1, :b 2} {:a :new-a, :b :new-b})
    (log/info "LinkedIn profile: " linkedInProfile)
    (p/setProfile (merge (p/getProfile state) {:linkedIn linkedInProfile}) ))
    )

(defn callback [params]
  (let [access_token (request-access-token (params "code"))
        state (params "state")]
    (profile-request access_token state)
    (str "<html><head>"
         "<script language=\"javascript\" type=\"text/javascript\">
         const channel = new BroadcastChannel('Tokhimo0');
         channel.postMessage('LinkedInclosed');
         setTimeout(\"self.close()\",1000);</script>"
         "</head><body>Closing..</body><html>")
    )
  )
(defn login
  "Login view, if we get back some query params from
  LinkedIn, then we continue request the access token, and publishing
  my profile data, otherwise show the LinkedIn Sign In button"
  [params]
    (if-let [error (params "error")]
      (log/error "LinkedIn error: " error)
      (getLinkedinAuthUrl (params "address")))
    )