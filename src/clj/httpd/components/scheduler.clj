(ns httpd.components.scheduler
  (:require [clojure.tools.logging :as log]
            [clojure.core.async :refer [<! >! go-loop]]
            [clj-time.periodic :refer [periodic-seq]]
            [chime :refer [chime-ch]]
            [clj-time.core :as t]
            [clojure.core.async :as a]))

(defn mk-chime-ch
  "Create a channel that will chime every period seconds"
  [period]
  (log/infof "creating chime channel with period of %d seconds" period)
  (chime-ch (periodic-seq
              (-> 1 t/seconds t/from-now)
              (-> period t/seconds))))

(defn with-catch [f arg]
  (try
    (f arg)
    (catch Throwable e (log/error e e))))

(defn chime-task [ch f]
  (go-loop []
    (when-let [time (<! ch)]
      (with-catch f time)
      (recur)))
  ch)

(defn periodic-task [period f]
  (chime-task
    (mk-chime-ch period)
    f))

(defn periodic-task-cancelable [period f]
  (let [ch (mk-chime-ch period)]
    (chime-task ch (partial f ch))))

(defn cancel-task [x]
  (a/close! x))

(defn task [pseq f]
  (->
    (chime-ch pseq)
    (chime-task f)))