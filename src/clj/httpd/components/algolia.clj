(ns httpd.components.algolia
  (:require [httpd.utils :as u]
            [clojure.tools.logging :as log]
            [httpd.database :as p])
  (:import (com.algolia.search ApacheAPIClientBuilder)
           (HelloWeb3j TokenScheme)
    #_(java.util Arrays)
           (java.time.format DateTimeFormatter)
           (java.time Instant)))

(def index (atom nil))

(def client (.build (ApacheAPIClientBuilder. "3KOI7BIO5J" "8e089f41572ecc32e247600041ba8a52")))
;(def ^com.algolia.search.Index index (.initIndex client "index02" TokenScheme))

(defn initIndex [indexName]
  (reset! index (.initIndex client indexName TokenScheme))
  )

(defn addTokenInfo [params r]
  (let [tokenId (str (u/getTokenIdFromReceipt r))
        refNumber (-> (:description params)
                      (clojure.string/split #"\n|\s")
                      (first)
                      (Integer/parseInt))
        description (if (not (nil? refNumber))
                      (if-let [html (p/getContent (:address params) refNumber)]
                        (:html html)
                        (do
                          (log/warn (str "parse was ok but no data: " refNumber))
                          refNumber))
                      (do
                        (log/warn (str "RefNumber cannot be parsed: " params))
                        (:description params) ))
        ^Instant endTimeObj (Instant/from
                              (.parse DateTimeFormatter/ISO_OFFSET_DATE_TIME (:start params)))]
    (log/warn (str "algolia_posting: " tokenId "," (.getEpochSecond endTimeObj) ","
                   description "," params ))

    (.addObject @index (-> (TokenScheme.)
                           (.setObjectID tokenId)
                           (.setEventId (:id params))
                           (.setTitle (:title params))
                           (.setDescriptionId (str (:address params) "_" refNumber))
                           (.setDescription (str description))
                           (.setHostAddr (:address params))
                           (.setStartTime (:start params))
                           (.setStartTimeLong (.getEpochSecond endTimeObj))
                           (.setEndTime (:end params))
                        )))
  )

(defn delTokenInfoByTokenId [tokenId]
  (.deleteObject @index (str tokenId)))

(defn delTokenInfo [r]
  (.deleteObject @index (str (u/getTokenIdFromReceipt r))))

#_(defn -main
  "Application entry point"
  [& args]

  (let [client (.build (ApacheAPIClientBuilder. "3KOI7BIO5J" "8e089f41572ecc32e247600041ba8a52"))
        index (.initIndex client "index01" Contact)]

    (.addObjects index (Arrays/asList (to-array [(-> (new Contact)
                                                     (.setObjectID "myID1")
                                                     (.setName "Jimmie")
                                                     (.setAge 23)
                                                     )
                                                 (-> (new Contact)
                                                     (.setObjectID "myID2")
                                                     (.setName "Jiomie")
                                                     (.setAge 22)
                                                     )]
                                                )))

    ))