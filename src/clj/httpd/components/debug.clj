(ns httpd.components.debug
  (:use [hiccup.core])
  (:require [httpd.database :as p]))

(defn mkTable [x]
  (let [keys (keys x)]
    [:table
     (for [y (sort keys)]
       [:tr
        [:td y]
        [:td [:pre (hiccup.util/escape-html (str (get x y)))]]]
       )]
  ))

(defn view []
  (html [:div
         (mkTable (p/getProfiles))
         (mkTable (p/getEvents))
         (mkTable (p/getContentsAll))
         ])
  )