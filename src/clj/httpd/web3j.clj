(ns httpd.web3j
  (:gen-class)
  (:require [clojure.tools.logging :as log]
            [httpd.database :as p]
            [httpd.emails :as e]
            [httpd.utils :as u]
            [httpd.comm :as comm]
            [httpd.components.algolia :as al]
            [cheshire.core :as c])
  (:import [org.web3j.protocol Web3j]
           [org.web3j.protocol.http HttpService]
           [rx.functions Action1]
           [HelloWeb3j MyToken]
           (org.web3j.crypto WalletUtils)
           (org.web3j.utils Numeric)
           (java.io File)
           (java.nio ByteBuffer)
           (java.util Date)
           (java.time Instant))
  )

(def totalSupply (atom -1))
(def network_id "4")

(def tokenAddr (MyToken/getPreviouslyDeployedAddress network_id))

(defn from-wei
  [wei]
  (/ wei 1000000000000000000))

(defn eth->wei
  [eth]
  (biginteger (* eth 1000000000000000000)))

(defn creds []
  ;; If you have no json file.
  #_(WalletUtils/generateFullNewWalletFile "lala0054" (File. "wallet")) ;
  (let [password "lala0054"
        passwdPath (str "wallet/"
                        "UTC--2018-06-28T10-45-28.497000000Z--de560c41fc36aa02d0cb8161f15747701a30015d.json")
        passwdFileObj (File. passwdPath)]
    (if (and password passwdFileObj)
      (let [creds (WalletUtils/loadCredentials
                     password
                     passwdFileObj)]
        creds)
      (throw (ex-info "Make sure you provided proper credentials in appropriate resources/config.edn"
                      {:password password :file-path passwdFileObj})) )))

(defn create-web3j []
  (Web3j/build (new HttpService (case network_id
                                  "4" "https://rinkeby.infura.io/7QYvMaW8qdIAPws0c8E7"
                                  "http://localhost:8545") ))
  )
(def web3j (create-web3j))

(defn gas-price
  []
  (-> 20000000000 #_(:gas-price env 20000000000)            ;; 20 gwei default
      str
      BigInteger.))

(def mytoken (MyToken/load tokenAddr web3j
                           (creds)
                           (gas-price)
                           (BigInteger/valueOf 500000)))

(defn notifMint [info ^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (comm/postMail
    (e/mkMailBasic info r))
  (comm/postUpdate
    (e/mkEventAttendeeUpdate info r))
  ; merging tokenId with same eventId
  (comm/postEvent
    (e/mkEventForDatabase info r))
  ;;
  (al/addTokenInfo info r)
  )

(defn notifMove [event toAddr fromAddr r]
  (let [tokenEvent (merge (c/parse-string
                     (:result
                       (comm/postDupEvent (c/generate-string {:eventId       (:id event)
                                                              :tokenId       (:tokenId event)
                                                              :address       fromAddr
                                                              :newOwnerEmail (:email (p/getProfile toAddr))}))
                       ) true)
                     {:tokenId (:tokenId event) :oldId (:id event)})]
    (comm/postMail
      (e/mkMailMoveTo tokenEvent (:email (p/getProfile toAddr)) r))
    (comm/postMail
      (e/mkMailMoveFrom tokenEvent (:email (p/getProfile fromAddr)) r))
    (comm/deletEvent (c/generate-string {:id (:id event)
                                         :tokenId (:tokenId event)}))
    (comm/postEvent (c/generate-string tokenEvent #_(u/convGEventToMyJson tokenEvent (:email event) (:email event)))))
  ;; Duplicate run will be occurred if we use the line below....
  (al/delTokenInfoByTokenId (:tokenId event))
  )

(defn notifBurn [info r]
  (comm/postMail
    (e/mkMailBurn info r))
  ;;
  (al/delTokenInfo r)
  )

(defn mintTokenWithMsg [params msg]
  (let [addr (:address params)]
    (log/info (str "mintTokenWithMsg: " (:id params) "," msg "," addr))
    (if (not= msg "")
      (u/execMethodCall (partial notifMint (-> params
                                               (assoc :to (:email (p/getProfile addr)))))
                        ;; When minting token, its price for having the token is set as 1 ether.
                        ;; This price will be able to changed via MyPage of sellers.
                        (-> mytoken (.mintWithMsg msg (BigInteger. "1000000000000000") addr)))
      (do
        (log/info "Failed. No msg was assigned to mint.")
        {:status "NG" :result "No msg to store."})))
  )

(defn moveToken [event params]
  (let [fromAddr (:fromAddr params)
        tokenId (:tokenId params)
        toAddr (:toAddr params)
        transferHash (:txHash params)]

    (log/info (str "moveToken: " tokenId "," toAddr "," transferHash "," event)) ;TODO check transferHash!!
    ;
    ; do something about transferHash of buying
    ;
    (u/execMethodCall (partial notifMove event toAddr fromAddr)
                      (-> mytoken (.moveToken (BigInteger. (str tokenId))
                                              toAddr)))
    {:status "OK" :result (str "Token Transfer is registered. " fromAddr " -> " toAddr)}
    ))

(defn burnToken [info]
  (log/info (str "burnToken: " (:tokenId info) ", " info))
  (if-let [tokenId (:tokenId info)]
    (do (u/execMethodCall (partial notifBurn info)
                          (-> mytoken (.burn (BigInteger/valueOf tokenId))) )
        {:status "OK" :result "Burn started."})
    {:status "NG" :result "Burn failed because tokenId wasnt found."})
  )

(defn getServerAddr []
  (.getAddress (creds))
  )

;--- test ---
(def _callbackGetVersion
  (reify Action1
    (call [_ x] (println "received value:" x " from Action1"))))

(defn _getVersion []
    ;; async with RxJava?
    (-> web3j
        (.web3ClientVersion)
        (.observable)
        (.subscribe _callbackGetVersion))

    ;; sync
    #_(-> web3j
        (.web3ClientVersion)
        (.send)
        (.getWeb3ClientVersion))
  )

(defn- totalSupplySuccess [result]
  (reset! totalSupply result)
  (str "totalSupply: " @totalSupply)
  )

(defn getTotalSupply []
  (let []
    (u/execMethodCall (partial totalSupplySuccess)
                      (-> mytoken (.totalSupply)))
    {:status "OK" :result (str "totalSupply was issued. ")}
    ))

(defn- payByOverdueSuccess [tokenId result]
  (str "payByOverdueSuccess: " tokenId ", " result)
  )

(defn payByOverdue [tokenId]
  (let [hostP 97
        tokhP 3]
    (log/info (str "payByOverdue: " tokenId))
    (u/execMethodCall (partial payByOverdueSuccess tokenId)
                      (-> mytoken (.payByOverdue (BigInteger. (str tokenId))
                                                 (BigInteger. (str hostP))
                                                 (BigInteger. (str tokhP)))))
    {:status "OK" :result (str "payByOverdue was issued. " tokenId)}
    )
  )

(defn- tokenStartEpocSuccess [tokenId result]
  (let [nowInstant (.toInstant (Date.))
        nowEpocSec (.getEpochSecond nowInstant)
        overdue? (> nowEpocSec result)]
    (log/info (str "currentEpocSec: " nowEpocSec " vs " result " = " overdue?))
    (if (not (= 0 result))
      (if overdue?
        (payByOverdue tokenId)
        (log/info (str tokenId " isnt overdued.")) )
      (log/info (str "dueCheck for " tokenId " was failed."))) ; result is actual result from the call.
    )
  (str "startEpocSuccess" tokenId ", " result)
  )

(defn getTokenStartEpoc [tokenId]
  (let []
    (u/execMethodCall (partial tokenStartEpocSuccess tokenId)
                      (-> mytoken (.getStartEpocSec (BigInteger. (str tokenId)))))
    {:status "OK" :result (str "dueCheck was issued. " tokenId)}
    )
  )

(defn- setIgnoreDueSuccess [tokenId result]
  (str "payByOverdueSuccess: " tokenId ", " result)
  )

(defn setIgnoreDue [tokenId flag]
  (let []
    (log/info (str "setIgnoreDue: " tokenId))
    (u/execMethodCall (partial setIgnoreDueSuccess tokenId)
                      (-> mytoken (.setIgnoreDue (BigInteger. (str tokenId))
                                                 flag)))
    {:status "OK" :result (str "setIgnoreDue was issued. " tokenId)}
    )
  )