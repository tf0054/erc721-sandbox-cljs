(ns httpd.utils
  (:require [clojure.core.async :as a]
            [clojure.core.async :refer [<! >! go-loop]]
            [clojure.tools.logging :as log]
            [clj-time.periodic :refer [periodic-seq]]
            [clj-time.core :as t]
            [chime :refer [chime-ch]]
            [cheshire.core :as c]
            [httpd.comm :as comm]
            [httpd.components.scheduler :as scheduler]
            [httpd.database :as p])
  (:import (org.web3j.utils Numeric)
           (java.time.format DateTimeFormatter)
           (java.time Instant)))

(def txResult (atom {}))

(defn uuid [] (.toString (java.util.UUID/randomUUID)))

;----
(defn getCompletable [x]
  (org.web3j.utils.Async/run (fn [] (.send x) )))

(defn watchTx [func cf ch now]
  (let [status (.isDone ^java.util.concurrent.CompletableFuture cf)]
    (try
      (if status
        (let [res (.get ^java.util.concurrent.CompletableFuture cf)]
          (swap! txResult assoc (keyword (str (System/identityHashCode cf))) res)
          (scheduler/cancel-task ch)
          (log/info (str "watching-tx/cb: " (func res))))
        (log/info (str "watching-tx/status(" now "): " status)))
      (catch Exception e (log/error (str "watchTx: " (.getMessage e) "\n"
                                         (->> e .getStackTrace seq) ))))
    ))

(defn execMethodCall [func obj]
  #_(log/info (str "moveToken: " tokenId "," addr "," transferHash)) ;TODO check transferHash!!
  ;
  ; do something about transferHash of buying
  ;
    (let [^java.util.concurrent.CompletableFuture cf (getCompletable obj)]

      ;; asnyc watcher is kicked.
      (scheduler/periodic-task-cancelable 3 (partial watchTx func cf))

      {:status "OK" :result {:watchId (str (System/identityHashCode cf))}} )
  )

(defn getTxResult [watchId]
  (get @txResult (keyword watchId)) )

(defn removeTxResult [watchId]
  (swap! txResult dissoc (keyword watchId)) )

(defn getTokenIdFromReceipt [^org.web3j.protocol.core.methods.response.TransactionReceipt r]
  (let [data (.getData (.get (.getLogs r) 0))]
    (BigInteger. (Numeric/hexStringToByteArray data))
    ))

(defn convGEventToMyJson [x oEmail oAddr]
  {:id          (:id x)
   :htmlLink    (:htmlLink x)
   :email       oEmail
   :address     oAddr
   :start       (-> x :start :dateTime)
   :end         (-> x :end :dateTime)
   :title       (:summary x)
   :description (:description x)}
  )

(defn convToEpocSec [t]
  (let [^Instant endTimeObj (Instant/from
                              (.parse DateTimeFormatter/ISO_OFFSET_DATE_TIME t))]
    (.getEpochSecond endTimeObj)
    )
  )