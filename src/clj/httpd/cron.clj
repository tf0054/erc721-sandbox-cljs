(ns httpd.cron
  (:require [clojure.tools.logging :as log]
            [cheshire.core :as c]
            [httpd.comm :as comm]
            [httpd.database :as p]
            [httpd.utils :as u]
            [httpd.web3j :as w]
            [httpd.emails :as e]))

(def serverEmail "tf0053@gmail.com")

(defn created [event]
  (let [oEmail (-> event :creator :email)
        r (:result (c/parse-string (:body (comm/getProfile oEmail)) true))]
    (log/info (str "Calender was editted by: " oEmail "\n" event))
    (if-let [oAddr (:address r)]
      (let [myJsonReq (u/convGEventToMyJson event oEmail oAddr)]
        (if (= "OK" (:status (comm/postEvent (c/generate-string event))))
          (log/info (str "mint was registered: " (comm/postToken
                                                   (c/generate-string myJsonReq))))
          (log/warn (str "Duplicated event was found " (:id event) " -> ignored."))
          ))                                                ;TODO
      (log/warn "No address found for " oEmail " -> ignored.")) )
  )

(defn cancelled [event]
  (let [tokenId (-> event :extendedProperties :shared :tokenId)]
    (log/info (str "Event " (:id event) " / " tokenId " was cancelled.\n" event))

    ;(if (not (nil? tokenId))
      (let [extProps (-> event :extendedProperties :shared)
            emailHash {(-> extProps :seller) "seller"
                       (-> extProps :buyer)  "buyer"}
            decliners (doall (filter #(= (:responseStatus %) "declined") (-> event :attendees)))
            byWho (get emailHash (:email (first decliners)))]
        (if (not (nil? decliners))
          (do
            (log/warn (str (first decliners) " = " byWho))
            (comm/postMail (e/mkMailCancel (merge event {:to  (:email (first decliners))
                                                         :who byWho})))
            (comm/postMail (e/mkMailCancelled (merge event {:to  (if (= byWho "seller")
                                                                   (get extProps (keyword "buyer"))
                                                                   (get extProps (keyword "seller") ))
                                                            :who byWho})))
            ;;
            (if (= "OK" (:status (comm/deletEvent (c/generate-string {:id      (:id event)
                                                                      :tokenId (:tokenId event)}))))
              ;(if-let [eventInfo (p/getEvent (keyword (str "_" (:id event))))]
              ;  (log/info "burn was registered:" (comm/burnToken (c/generate-string eventInfo))))
              (log/warn "The event cannot be found on dbf." event)))
          (log/info "TODO: nothing done in cancellation process")
          )
        )
      #_(log/info (str "non-tokenEvent(available timeslot event) was deleted (normal)"))
      ))
  ;)

(defn checkCalendar [x]
  (let [res (comm/getCalenderSync)
        resClj (c/parse-string (:body res) true)]
    (log/debug (.toString x) resClj)
    (if (not (empty? (-> resClj :result)))
      (let [items (-> resClj :result)]
        (doall (for [dbReq items
                     :let [oEmail (-> dbReq :creator :email)]]
                 (if (= oEmail serverEmail)
                   ;; tokenEvent was edited.
                   (cancelled dbReq)
                   ;; availavle slot resigtration event was edited.
                   (if (not= "cancelled" (-> dbReq :status))
                     (created dbReq)
                     (log/warn (str "non tokenevent was cancelled. -> ignored." dbReq)))
                   )))
      #_(log/warn "No updates found on calendar.")
      )
    )
  ))

(defn checkPaymentDue [t]
  ;; update totalSupply
  (w/getTotalSupply)
  ;; check all startEpocSec
  (let [totalTokenNum (deref w/totalSupply)]
    (if (not (= -1 totalTokenNum))
      (doall (for [tId (range totalTokenNum)]
               (w/getTokenStartEpoc tId)
               ))
      (log/info (str "checkPaymentDue was skipped. " t " " totalTokenNum))
      )
    )
  )