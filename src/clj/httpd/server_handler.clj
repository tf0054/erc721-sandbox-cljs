(ns httpd.server-handler
  (:require [clojure.java.io :as io]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.json :refer [wrap-json-response
                                          wrap-json-params]]
            [ring.middleware.gzip :refer [wrap-gzip]]
            [ring.logger :as logger]
            [ring.util.response :as response]
            [cheshire.core :as c]
            [clj-time.core :as t]
            [clj-time.periodic :refer [periodic-seq]]
    ;;
            [httpd.web3j :as web3]
            [httpd.comm :as comm]
            [httpd.cron :as cron]
            [httpd.database :as p]
            [httpd.components.codax :as co]
            [httpd.components.algolia :as al]
            [httpd.components.scheduler :as scheduler]
            [httpd.components.linkedin :as linkedin]
            [httpd.components.debug :as debug]
            [clojure.tools.logging :as log]
            [httpd.utils :as u]))

;; If you are new to using Clojure as an HTTP server please take your
;; time and study ring and compojure. They are both very popular and
;; accessible Clojure libraries.

;; --> https://github.com/ring-clojure/ring
;; --> https://github.com/weavejester/compojure

(defroutes app-routes
           ;; NOTE: this will deliver all of your assets from the public directory
           ;; of resources i.e. resources/public
           (route/resources "/" {:root "public"})
           ;; NOTE: this will deliver your index.html
           (GET "/" [req]
             (do
               (log/info (str "/ " req))
               (-> (response/resource-response "index.html" {:root "public"})
                   (response/content-type "text/html"))))
           (GET "/serverInfo" []
             {:body {:address (web3/getServerAddr)
                     :network (Integer/parseInt web3/network_id)
                     :tokenAddr web3/tokenAddr}})
           (POST "/notifications" req
             (log/info (str "From google (webhook). " req))
             (let [headers (:headers req)]
               (spit "/tmp/notification.log" (str headers "\n") :append true)
               #_(log/info (str "Resource_id. " (get headers "x-goog-resource-id")))
               {:body {:status "OK"}}))
           ;; --
           (POST "/profile" req
             (let [params (clojure.walk/keywordize-keys (:params req))]
               (if (> (count (keys params)) 2)
                 (let [res (p/setProfile params)]
                   (log/info (str "setting profile. " params))
                   (spit "/tmp/profiles.log" (str (p/getProfiles) "\n") :append true)
                   {:body res})
                 {:body {:status "NG" :result "needed columns arent found."}} )
               ))
           (GET "/profile" req
             ;TODO: email is also assigned on :address
             (let [params (:params req)
                   res (p/getProfile (:address params))]
               (log/info (str "getting profile: " (:address params)))
               (if (< 3 (count (keys res)))
                 {:body {:status "OK" :result res}})))
           (DELETE "/profile" req
             (let [params (clojure.walk/keywordize-keys (:params req))]
               (log/warn "del profile:" params)
               {:body (p/delProfile params)} ))
           ;; --
           (POST "/event" req
             (let [params (clojure.walk/keywordize-keys (:params req))]
                 {:body (p/setEvent params)}
               ))
           (GET "/event" req
             (let [params (:params req)
                   res (p/getEvent (keyword (:id params)))]
               (log/info (str "getting event:" (:id params)))
               {:body {:status "OK" :result res}}))
           (DELETE "/event" req
             (let [params (clojure.walk/keywordize-keys (:params req))]
               (log/warn "del event:" params)
               {:body (p/delEvent params)} ))
           ;; --
           (GET "/txResult" req
             (log/info (str "getting tx result. " (:params req)))
             {:body {:result (.toString (u/getTxResult (:watchId (:params req))))}})
           (POST "/createToken" req
             (let [params (clojure.walk/keywordize-keys (:params req))
                   apiResponse (comm/postJson (comm/convJson (:params req)))
                   res (web3/mintTokenWithMsg
                         params
                         (get (c/parse-string (:body apiResponse)) "uri") )]
               {:body res}
               ))
           (POST "/moveToken" req
             (log/info (str "TODO: txHash check needed on moveToken. " (:params req)))
             (let [params (clojure.walk/keywordize-keys (:params req))]
               (log/info (str "/moveToken " params))
               (let [event (p/getEvent (keyword (str (:tokenId params))))
                     res (web3/moveToken event params)]
                 {:body res})))
           (POST "/cancelToken" req
             (let [params (clojure.walk/keywordize-keys (:params req))
                   res (web3/burnToken params)]
               {:body res}
               ))
           ;;
           (GET "/content" req
             (let [params (:params req)
                   res (p/getContent (:address params) (:contentId params))]
               (log/info (str "getting content: " params res))
               {:body {:status "OK" :result res}}))
           (GET "/contents" req
             (let [params (:params req)
                   res (p/getContents (:address params))]
               (log/info (str "getting contents: " params))
               {:body {:status "OK" :result res}}))
           (POST "/content" req
             (let [params (clojure.walk/keywordize-keys (:params req))
                   res (p/setContent params)]
               {:body res}
               ))
           (DELETE "/content" req
             (let [params (clojure.walk/keywordize-keys (:params req))
                   res (p/delContent params)]
               (log/warn "del content:" params)
               {:body {:status "OK" :result res :id (:id params) :category "content"}} ))
           ;; --
           (POST "/imgupload" {{{tempfile :tempfile filename :filename} :file} :params :as params}
             (io/copy tempfile (io/file "resources" "public" "static" "uploaded"
                                        filename))
             "Success")
           ;;
           (GET "/linkedin" {params :query-params}
             (if-let [authorization_token (params "code")]
               (linkedin/callback params)
               {:body {:status "OK" :result (linkedin/login params)}} ))
           ;; --
           (GET "/debug" req
             (if (= "0054" (:key (:params req)))
               (do
                 (log/info (str "debug. " (:params req)))
                 #_{:body {:status "OK" :result {:profiles (p/getProfiles)
                                               :events   (p/getEvents)
                                               }}}
                 (debug/view)
                 )
               ))
           ;;
           (route/resources "/static")
           (route/not-found "Not Found"))

(def app (wrap-defaults (wrap-json-response app-routes)
                        (assoc-in site-defaults [:security :anti-forgery] false)
                        ))

;; this development application has a var reference to the app-routes above
;; for friendlier REPL based reloading
(def dev-app (wrap-gzip
               (wrap-defaults
                 (wrap-json-params
                   (wrap-json-response
                     (logger/wrap-with-logger
                       app-routes)))
                 (-> site-defaults
                     (assoc-in [:security :anti-forgery] false)
                     (assoc-in [:params :multipart] true)
                     (assoc-in [:session :cookie-attrs :secure] true)
                     (assoc-in [:session :cookie-name] "secure-ring-session")
                     )))
  )

(co/opendbf)
(al/initIndex web3/tokenAddr)
(scheduler/periodic-task 60 cron/checkCalendar)
(scheduler/periodic-task 60 cron/checkPaymentDue)