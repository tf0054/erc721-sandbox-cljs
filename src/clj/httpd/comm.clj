(ns httpd.comm
  (:require [clj-http.client :as client]
            [clojure.tools.logging :as log]
            [cheshire.core :as c])
  )

(def jsonStoreApi "https://api.myjson.com")

(defn wrapHttpRes [res]
  (if (= 200 (:status res))
    (do (log/warn "OK:" (:body res))
        (c/parse-string (:body res) true))
    (do (log/warn "ERR:" res)
        nil)
    ))

(defn postJson [rawStr]
    (client/post (str jsonStoreApi "/bins") {:content-type "application/json"
                                             :body         rawStr}))

(defn convJson [rawStr]
  (let [cljObj rawStr
        newObj
    (-> cljObj
        ;; add
        (assoc "name" (if (contains? cljObj "title")
                        (get cljObj "title")
                        (get cljObj "text"))) ;;event name on dayplot calendar
        (assoc "image" "https://image.flaticon.com/icons/png/512/681/681662.png")
        (assoc "description" (str (get cljObj "description") ", "
                                  (get cljObj "start") "-" (get cljObj "end") ", "
                                  (get cljObj "id")))
        (assoc "external_url" (get cljObj "htmlLink"))
        (assoc "background_color" "00FFFF")
        ;; delete
        (dissoc "address")
        (dissoc "network")
        (dissoc "tag")
        (dissoc "text")
        #_(dissoc "start")
        #_(dissoc "end")
        #_(dissoc "id")
        (dissoc "htmlLink")
        (dissoc "value") )]
    (c/generate-string newObj))
  )

;; opensea.io
;
;{
; "name": "Dave McPufflestein",
;       "image": "https://storage.googleapis.com/opensea-prod.appspot.com/puffs/3.png",
; "description": "Generic puff description. This really should be customized.",
; "attributes": {
;                "level": 3,
;                       "weapon_power": 55,
;                "personality": "sad",
;                "stamina": 11.7
;                },
; "external_url": "https://cryptopuff.io/3",
; "background_color": "00FFFF"
; }

(defn getCalenderSync []
  (client/get (str "http://localhost:4000" "/sync")
              {:content-type "application/json"
               :body         ""}))

(defn postMail [rawStr]
  (client/post (str "http://localhost:4000" "/send")
               {:content-type "application/json"
                :body         rawStr}))

(defn postUpdate [rawStr]
  (client/post (str "http://localhost:4000" "/update")
               {:content-type "application/json"
                :body         rawStr}))

(defn postDupEvent [rawStr]
  (wrapHttpRes (client/post (str "http://localhost:4000" "/dupevent")
               {:content-type "application/json"
                :body         rawStr})))

(defn getProfile [x]
  (if (> (count x) 0)
    (client/get "http://localhost:8080/profile"
                {:content-type "application/json"
                 :query-params {:address x}})
    nil
    ))

(defn getEvent [x]
  (wrapHttpRes (client/get "http://localhost:8080/event"
               {:content-type "application/json"
                :query-params {:id x}})))

(defn postEvent [rawStr]
  (wrapHttpRes (client/post "http://localhost:8080/event"
               {:content-type "application/json"
                :body         rawStr})))

(defn deletEvent [rawStr]
  (wrapHttpRes (client/delete "http://localhost:8080/event"
                           {:content-type "application/json"
                            :body         rawStr})))

(defn postToken [rawStr]
  (wrapHttpRes (client/post "http://localhost:8080/createToken"
               {:content-type "application/json"
                :body         rawStr})))

(defn burnToken [rawStr]
  (wrapHttpRes (client/post "http://localhost:8080/cancelToken"
                              {:content-type "application/json"
                :body         rawStr})))