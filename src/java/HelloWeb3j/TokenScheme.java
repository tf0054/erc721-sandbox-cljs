package HelloWeb3j;

public class TokenScheme {

    private String ObjectID;
    private String description;
    private String descriptionId;

    public String getTitle() {
        return title;
    }

    public TokenScheme setTitle(String title) {
        this.title = title;
        return this;
    }

    private String title;
    private String hostAddr;
    private String startTime;
    private long startTimeLong;
    private String endTime;

    public String getEventId() {
        return eventId;
    }

    public TokenScheme setEventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    private String eventId;

    public String getStartTime() {
        return startTime;
    }

    public TokenScheme setStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    public long getStartTimeLong() {
        return startTimeLong;
    }

    public TokenScheme setStartTimeLong(long startTime) {
        this.startTimeLong = startTime;
        return this;
    }

    public String getEndTime() {
        return endTime;
    }

    public TokenScheme setEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    public String getObjectID() {
        return ObjectID;
    }

    public TokenScheme setObjectID(String id) {
        this.ObjectID = id;
        return this;
    }

    public TokenScheme() {}

    public String getDescriptionId() {
        return descriptionId;
    }

    public TokenScheme setDescriptionId(String name) {
        this.descriptionId = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public TokenScheme setDescription(String name) {
        this.description = name;
        return this;
    }

    public String getHostAddr() {
        return hostAddr;
    }

    public TokenScheme setHostAddr(String name) {
        this.hostAddr = name;
        return this;
    }
}
