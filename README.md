This directory has cljs codes which are build on webpacked js for realizing ethereum communication via drizzle.

And you can find the codes of the bundled js and truffle files (compile contracts to JSON and upload the build to chain)

### For deploying contracts
```
cd truffle
npm install
truffle migrate
```

### For making bundled js
```
cd webpack
npm install
npm run mklib
npm run cplib
```

### For cljs dev
```
cp -pir target.keep target
lein figwheel
```
